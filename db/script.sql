insert into categoria (id, nome) values (nextval('categoria_seq'), 'Alimentação');
insert into categoria (id, nome) values (nextval('categoria_seq'), 'Carro');
insert into categoria (id, nome) values (nextval('categoria_seq'), 'Empréstimo');
insert into categoria (id, nome) values (nextval('categoria_seq'), 'Empréstimo para Outros');
insert into categoria (id, nome) values (nextval('categoria_seq'), 'Lazer');
insert into categoria (id, nome) values (nextval('categoria_seq'), 'Salário');
insert into categoria (id, nome) values (nextval('categoria_seq'), 'Telefone & Internet');
insert into categoria (id, nome) values (nextval('categoria_seq'), 'O Boticário');
insert into categoria (id, nome) values (nextval('categoria_seq'), 'MimosDonAna');
insert into categoria (id, nome) values (nextval('categoria_seq'), 'Manutenções da casa');

insert into tipo_pagamento (id, nome) values (nextval('tipo_pagamento_seq'), 'Cartão de crédito Caixa');
insert into tipo_pagamento (id, nome) values (nextval('tipo_pagamento_seq'), 'Cartão de crédito Hipercard');
insert into tipo_pagamento (id, nome) values (nextval('tipo_pagamento_seq'), 'Cartão de crédito Submarino');
insert into tipo_pagamento (id, nome) values (nextval('tipo_pagamento_seq'), 'Cartão de débito');
insert into tipo_pagamento (id, nome) values (nextval('tipo_pagamento_seq'), 'Diheiro');
insert into tipo_pagamento (id, nome) values (nextval('tipo_pagamento_seq'), 'Cheque');