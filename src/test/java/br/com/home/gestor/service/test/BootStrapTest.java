package br.com.home.gestor.service.test;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import br.com.home.gestor.service.LancamentoService;

@ContextConfiguration(locations={"/context.xml"})
public class BootStrapTest extends  AbstractJUnit4SpringContextTests{

	@Inject
	private LancamentoService lancamentoService;
	
	@Test
	public void fooTest(){
		
		System.out.println("funcionando !!" + lancamentoService);
	}
}
