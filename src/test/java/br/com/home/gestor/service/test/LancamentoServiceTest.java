package br.com.home.gestor.service.test;

import java.math.BigDecimal;
import java.util.Date;

import javax.inject.Inject;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import br.com.home.gestor.modelo.Lancamento;
import br.com.home.gestor.modelo.PeriodicidadeParcelada;
import br.com.home.gestor.modelo.TipoLancamento;
import br.com.home.gestor.service.LancamentoService;

import static org.junit.Assert.*;


@ContextConfiguration(locations={"/context.xml"})
public class LancamentoServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Inject
	private LancamentoService lancamentoService;
	
	
	@Test
	public void salvarTest(){
		
		Lancamento lancamento = Lancamento.novo();
		lancamento.setDataLancamento(new Date());
		lancamento.setDescricao("Compra supermercado Mister Jr");
		lancamento.setTipoLancamento(TipoLancamento.SAIDA);
		lancamento.setValor(BigDecimal.valueOf(83.59));
		
		lancamentoService.save(lancamento);
	}
	
	@Test
	public void lancamentoParceladoSaidaTest(){
		
		Lancamento lancamento = Lancamento.novo();
		lancamento.setDataLancamento(new Date());
		lancamento.setDescricao("Sofá Magazine Luiza");
		lancamento.setTipoLancamento(TipoLancamento.SAIDA);
		lancamento.setPeriodicidadeParcelada(PeriodicidadeParcelada.MESES);
		lancamento.setValor(new BigDecimal("100"));
		
		lancamentoService.registraLancamentoParcelado(lancamento, 1, 10);
	}
	
	@Test
	public void lancamentoParceladoEntradaTest(){
		
		Lancamento lancamento = Lancamento.novo();
		lancamento.setDataLancamento(new Date());
		lancamento.setDescricao("Pagamento de processo");
		lancamento.setTipoLancamento(TipoLancamento.ENTRADA);
		lancamento.setPeriodicidadeParcelada(PeriodicidadeParcelada.MESES);
		lancamento.setValor(new BigDecimal("1500.81"));
		
		lancamentoService.registraLancamentoParcelado(lancamento, 1, 10);
	}
	
	@Test
	public void findByDataLancamentoTest(){
		
		LocalDate data = new LocalDate(2014,11,1);
		assertTrue("Deve encontar registro", !lancamentoService.findLancamentosNoMes(data.toDateMidnight().toDate()).isEmpty());
	}
}
