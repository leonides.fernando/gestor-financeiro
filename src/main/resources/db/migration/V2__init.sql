--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 9.3.4
-- Started on 2014-07-11 16:13:50

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 186 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2024 (class 0 OID 0)
-- Dependencies: 186
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 171 (class 1259 OID 50595)
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categoria (
    id bigint NOT NULL,
    nome character varying(32)
);


ALTER TABLE public.categoria OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 50652)
-- Name: categoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categoria_seq OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 50600)
-- Name: extrato; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE extrato (
    id bigint NOT NULL,
    data timestamp without time zone,
    saldo numeric(12,6)
);


ALTER TABLE public.extrato OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 50654)
-- Name: extrato_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE extrato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extrato_seq OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 50605)
-- Name: fundo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE fundo (
    id bigint NOT NULL,
    dataaplicacao date,
    nome character varying(128),
    valoraplicado numeric(12,6)
);


ALTER TABLE public.fundo OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 50656)
-- Name: fundo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE fundo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fundo_seq OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 50610)
-- Name: item_desejo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE item_desejo (
    id bigint NOT NULL,
    dataaquisicao timestamp without time zone,
    datacriacao timestamp without time zone,
    nome character varying(128),
    status character varying(64),
    valor numeric(21,6)
);


ALTER TABLE public.item_desejo OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 50658)
-- Name: item_desejo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE item_desejo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item_desejo_seq OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 50615)
-- Name: lancamento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE lancamento (
    id bigint NOT NULL,
    datalancamento date,
    descricao character varying(64),
    numeroparcela integer,
    pago boolean,
    periodicidadefixa character varying(32),
    periodicidadeparcelada character varying(32),
    tipolancamento character varying(32),
    valor numeric(12,6),
    tipopagamento_id bigint
);


ALTER TABLE public.lancamento OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 50620)
-- Name: lancamento_categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE lancamento_categoria (
    lancamento_id bigint NOT NULL,
    categoria_id bigint NOT NULL
);


ALTER TABLE public.lancamento_categoria OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 50660)
-- Name: lancamento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE lancamento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lancamento_seq OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 50625)
-- Name: saldo_consolidado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE saldo_consolidado (
    id bigint NOT NULL,
    ano integer,
    mes integer,
    valor numeric(12,6)
);


ALTER TABLE public.saldo_consolidado OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 50662)
-- Name: saldo_consolidado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE saldo_consolidado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.saldo_consolidado_seq OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 50630)
-- Name: tipo_pagamento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipo_pagamento (
    id bigint NOT NULL,
    nome character varying(64) NOT NULL
);


ALTER TABLE public.tipo_pagamento OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 50664)
-- Name: tipo_pagamento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipo_pagamento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_pagamento_seq OWNER TO postgres;

--
-- TOC entry 2002 (class 0 OID 50595)
-- Dependencies: 171
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--


--
-- TOC entry 2025 (class 0 OID 0)
-- Dependencies: 179
-- Name: categoria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categoria_seq', 1, false);


--
-- TOC entry 2003 (class 0 OID 50600)
-- Dependencies: 172
-- Data for Name: extrato; Type: TABLE DATA; Schema: public; Owner: postgres
--


--
-- TOC entry 2026 (class 0 OID 0)
-- Dependencies: 180
-- Name: extrato_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('extrato_seq', 1, false);


--
-- TOC entry 2004 (class 0 OID 50605)
-- Dependencies: 173
-- Data for Name: fundo; Type: TABLE DATA; Schema: public; Owner: postgres
--


--
-- TOC entry 2027 (class 0 OID 0)
-- Dependencies: 181
-- Name: fundo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('fundo_seq', 1, false);


--
-- TOC entry 2005 (class 0 OID 50610)
-- Dependencies: 174
-- Data for Name: item_desejo; Type: TABLE DATA; Schema: public; Owner: postgres
--


--
-- TOC entry 2028 (class 0 OID 0)
-- Dependencies: 182
-- Name: item_desejo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('item_desejo_seq', 1, false);


--
-- TOC entry 2006 (class 0 OID 50615)
-- Dependencies: 175
-- Data for Name: lancamento; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2007 (class 0 OID 50620)
-- Dependencies: 176
-- Data for Name: lancamento_categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2029 (class 0 OID 0)
-- Dependencies: 183
-- Name: lancamento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('lancamento_seq', 1, false);


--
-- TOC entry 2008 (class 0 OID 50625)
-- Dependencies: 177
-- Data for Name: saldo_consolidado; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2030 (class 0 OID 0)
-- Dependencies: 184
-- Name: saldo_consolidado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('saldo_consolidado_seq', 1, false);


--
-- TOC entry 2009 (class 0 OID 50630)
-- Dependencies: 178
-- Data for Name: tipo_pagamento; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2031 (class 0 OID 0)
-- Dependencies: 185
-- Name: tipo_pagamento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipo_pagamento_seq', 1, false);


--
-- TOC entry 1874 (class 2606 OID 50599)
-- Name: categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY (id);


--
-- TOC entry 1876 (class 2606 OID 50604)
-- Name: extrato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY extrato
    ADD CONSTRAINT extrato_pkey PRIMARY KEY (id);


--
-- TOC entry 1878 (class 2606 OID 50609)
-- Name: fundo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY fundo
    ADD CONSTRAINT fundo_pkey PRIMARY KEY (id);


--
-- TOC entry 1880 (class 2606 OID 50614)
-- Name: item_desejo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY item_desejo
    ADD CONSTRAINT item_desejo_pkey PRIMARY KEY (id);


--
-- TOC entry 1884 (class 2606 OID 50624)
-- Name: lancamento_categoria_categoria_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY lancamento_categoria
    ADD CONSTRAINT lancamento_categoria_categoria_id_key UNIQUE (categoria_id);


--
-- TOC entry 1882 (class 2606 OID 50619)
-- Name: lancamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY lancamento
    ADD CONSTRAINT lancamento_pkey PRIMARY KEY (id);


--
-- TOC entry 1886 (class 2606 OID 50629)
-- Name: saldo_consolidado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY saldo_consolidado
    ADD CONSTRAINT saldo_consolidado_pkey PRIMARY KEY (id);


--
-- TOC entry 1888 (class 2606 OID 50636)
-- Name: tipo_pagamento_nome_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipo_pagamento
    ADD CONSTRAINT tipo_pagamento_nome_key UNIQUE (nome);


--
-- TOC entry 1890 (class 2606 OID 50634)
-- Name: tipo_pagamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipo_pagamento
    ADD CONSTRAINT tipo_pagamento_pkey PRIMARY KEY (id);


--
-- TOC entry 1891 (class 2606 OID 50637)
-- Name: fk69a57a9c614c5a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lancamento
    ADD CONSTRAINT fk69a57a9c614c5a FOREIGN KEY (tipopagamento_id) REFERENCES tipo_pagamento(id);


--
-- TOC entry 1893 (class 2606 OID 50647)
-- Name: fkb1f270ae429d28da; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lancamento_categoria
    ADD CONSTRAINT fkb1f270ae429d28da FOREIGN KEY (categoria_id) REFERENCES categoria(id);


--
-- TOC entry 1892 (class 2606 OID 50642)
-- Name: fkb1f270aed1639f7a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lancamento_categoria
    ADD CONSTRAINT fkb1f270aed1639f7a FOREIGN KEY (lancamento_id) REFERENCES lancamento(id);


--
-- TOC entry 2023 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-07-11 16:13:50

--
-- PostgreSQL database dump complete
--

