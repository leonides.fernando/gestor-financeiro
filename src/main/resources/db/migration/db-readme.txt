﻿1. Rode inicialmente, na linha de comando e a partir da pasta do módulo que contém as entidades, o comando maven para 
criação da tabela de versionamento utilizado pelo flyway. Atente-se aos parâmetros que indicam URL de conexão, usuário 
e senha do banco.

1.1. Caso o banco de dados esteja desprovido das tabelas do sistema (zerado), utilize:

	mvn com.googlecode.flyway:flyway-maven-plugin:1.7:init -Dflyway.driver=org.postgresql.Driver -Dflyway.url=jdbc:postgresql://localhost:5432/gestor -Dflyway.user=postgres -Dflyway.password=postgres -Dflyway.initialDescription="Base version"

1.2. Caso o banco já contenha as tabelas do sistema, sem quaisquer dados, utilize:

	mvn com.googlecode.flyway:flyway-maven-plugin:1.7:init -Dflyway.driver=org.postgresql.Driver -Dflyway.url=jdbc:postgresql://localhost:5432/gestor -Dflyway.user=postgres -Dflyway.password=postgres -Dflyway.initialDescription="Base schema" -Dflyway.initialVersion=1

1.3. Caso o banco esteja em uma versão conhecida, indique os parâmetros de versão inicial:
	
	-Dflyway.initialDescription="<DESCRICAO_DA_VERSAO_CONHECIDA>" -Dflyway.initialVersion=<VERSAO_CONHECIDA>