create table configuracao(
	id bigint not null,
	quantidade_meses_media_gastos integer not null,
	CONSTRAINT configuracao_pkey PRIMARY KEY (id)	
);

create sequence configuracao_seq;

insert into configuracao (id, quantidade_meses_media_gastos) values (nextval('configuracao_seq'), 3);