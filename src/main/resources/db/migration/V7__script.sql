create table lancamento_fixo(
  id bigint NOT NULL,
  descricao character varying(64) not null,
  tipo_lancamento character varying(32) not null,
  periodicidade_fixa character varying(32) not null,
  valor numeric(12,6) not null,
  tipo_pagamento_id bigint not null,
  ativo boolean not null,
  mes_inicio INT  not null,
  ano_inicio INT not null,
  data_inicio date not null,
  data_termino date,
  data_ultimo_lancamento date not null,
  parcelas int not null,
  CONSTRAINT lancamento_fixo_pkey PRIMARY KEY (id),
  CONSTRAINT lancamento_fixo__tipo_pagamento_fk FOREIGN KEY (tipo_pagamento_id)
      REFERENCES tipo_pagamento (id)
);

CREATE TABLE lancamento_fixo_categoria
(
  lancamento_fixo_id bigint NOT NULL,
  categoria_id bigint NOT NULL,
  CONSTRAINT lancamento_fixo_categoria_pkey PRIMARY KEY (lancamento_fixo_id, categoria_id),
  CONSTRAINT lancamento_fixo__categoria_fk FOREIGN KEY (categoria_id)
      REFERENCES categoria (id) ,
  CONSTRAINT lancamento_fixo_categoria__lancamento_fixo_fk FOREIGN KEY (lancamento_fixo_id)
      REFERENCES lancamento_fixo (id)
);
