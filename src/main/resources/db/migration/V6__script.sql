
create table lancamento2(
  id bigint NOT NULL,
  datalancamento date,
  descricao character varying(64),
  numeroparcela integer,
  pago boolean,
  periodicidadefixa character varying(32),
  periodicidadeparcelada character varying(32),
  tipolancamento character varying(32),
  valor numeric(12,6),
  tipopagamento_id bigint,
  mes integer,
  ano integer,
  CONSTRAINT lancamento2_pkey PRIMARY KEY (id)
);

  insert into lancamento2  
(id,datalancamento,descricao,numeroparcela,pago,periodicidadefixa,periodicidadeparcelada,tipolancamento,valor,tipopagamento_id,mes,ano)
select *, date_part('month',datalancamento), date_part('year',datalancamento)
from lancamento;

alter table lancamento add column mes integer;
alter table lancamento add column ano integer;

update lancamento  set
mes = (select lancamento2.mes from lancamento2  where lancamento.id = lancamento2.id),
ano = (select lancamento2.ano from lancamento2  where lancamento.id = lancamento2.id);

drop table lancamento2;