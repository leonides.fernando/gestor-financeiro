
function showHideMany(ids){
	var idArray = ids.split(',');
	for(var i = 0; i < idArray.length; i++){
		showHide(idArray[i]);
	}
}

function showHide(id){
	var obj = document.getElementById(id);
	if(obj.style.display == 'none'){
		obj.style.display = 'block';
	}else{
		obj.style.display = 'none';
	}
}

function show(id){
	var obj = document.getElementById(id);
	obj.style.display = 'block';
}

function hide(id){
	var obj = document.getElementById(id);
	obj.style.display = 'none';
}
