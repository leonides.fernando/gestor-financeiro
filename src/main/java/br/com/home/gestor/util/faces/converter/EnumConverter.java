package br.com.home.gestor.util.faces.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "enumConverter")
public class EnumConverter implements Converter {

        private static final String ENUM_TYPE = "EnumConverter.enumType";

        @SuppressWarnings({ "rawtypes", "unchecked" })
        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String enumName) {
                
                if(enumName == null) {
                        
                        return null;
                }
                
                Class<Enum> enumType = (Class<Enum>) component.getAttributes().get(ENUM_TYPE);
                
                try {
                        
                        return Enum.valueOf(enumType, enumName);
                        
                } catch (IllegalArgumentException illegalArgumentException) {
                        
                        // o enum não possui constante com o nome especificado  
                        return null;                        
                } 
                
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object enumValue) {
                
                if(enumValue == null) {
                        return null;
                }
                
                if (enumValue instanceof Enum) {
                        
                        component.getAttributes().put(ENUM_TYPE, enumValue.getClass());
                        
                        return ((Enum<?>) enumValue).name();
                        
                } else {
                        
                        throw new ConverterException(new FacesMessage(String.format("%s não é do enum", enumValue.getClass())));
                        
                }
        }

}