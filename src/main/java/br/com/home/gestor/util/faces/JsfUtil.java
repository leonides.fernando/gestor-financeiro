package br.com.home.gestor.util.faces;


import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.servlet.http.HttpServletRequest;

import com.ocpsoft.pretty.PrettyContext;
import com.ocpsoft.pretty.faces.config.mapping.UrlMapping;

public class JsfUtil {

	public static void addInfoMessage(String message){
		getContext().addMessage(null, 
				new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
	}
	
	public static void addErrorMessage(String message){
		
		getContext().addMessage(null, 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
	}
	
	public static void addWarMessage(String message){
		
		getContext().addMessage(null, 
				new FacesMessage(FacesMessage.SEVERITY_WARN, message, null));
	}
	
	public static Flash flash(){
		return getContext().getExternalContext().getFlash();
	}

    private static FacesContext getContext() {
        
        return FacesContext.getCurrentInstance();
    }
	
	/**
	 * <p>Redireciona para a url especificada armazenando, caso existam, os parâmetros em {@link Flash}.</p>
	 * 
	 * <p><strong>ATENÇÃO: </strong></p> A versão 2.1.13 de <code>javax.faces</code>, utilizada no projeto, possui um 
	 * BUG conhecido que prejudica os parâmetros armazenados em {@link Flash} quando é alterada a URL relativa. Este BUG
	 * foi corrigido na versão 2.1.14 (ainda não disponível). Para utilização eficaz com parâmetros, redirecione 
	 * apenas para mesma URL relativa.</p>
	 * <blockquote>
	 * <strong>Exemplo:</strong><br/>
	 * <strong>De: </strong><code>/teste/</code>&nbsp;&nbsp;&nbsp;<strong>Para: </strong><code>/erro/</code>&nbsp;&nbsp;&nbsp;<strong>Flash: </strong>vazio.<br/>
	 * <strong>De: </strong><code>/teste/</code>&nbsp;&nbsp;&nbsp;<strong>Para: </strong><code>/teste/erro</code>&nbsp;&nbsp;&nbsp;<strong>Flash: </strong>OK.
	 * </blockquote>
	 * 
	 * @param url URL para redirecionamento.
	 * @param params Parâmetros a serem armazenados em {@link Flash}.
	 */
	public static void redirect(String url, Object... params) {
		
		try { 
			
			ExternalContext externalContext = getContext().getExternalContext();
			
			int paramsLength = (params != null && params.length > 0) ? params.length/2 : 0;
			
			if(paramsLength > 0) {
				
				Flash flash = externalContext.getFlash();
				
				for(int paramIndex = 1; paramIndex < params.length; paramIndex = paramIndex + 2) {
					
					flash.put((String)params[paramIndex - 1], params[paramIndex]);
				}
			}
			externalContext.redirect(url); 
			
		} catch(IOException e) {
			
		}
	}
	
	/**
     * <p>Obtém a URL resolvida de acordo com o contexto {@link PrettyContext pretty}, adiciona parâmetros.</p>
     * 
     * <p>Os parâmetros devem ser dispostos aos pares no formato ({@link String chave}, {@link Object valor}).</p>
     * 
     * @param url URL resolvida de acordo com o contexto {@link PrettyContext pretty}
     * @param params Parâmetros a serem adicionados a URL
     */
    public static String prettyURL(String prettyId, Object... params) {
            
            StringBuffer url = new StringBuffer();
            
            ExternalContext externalContext = getContext().getExternalContext();
            HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
            
            PrettyContext prettyContext = PrettyContext.getCurrentInstance(request);
            
            UrlMapping mapping = prettyContext.getConfig().getMappingById(prettyId);
            
            url.append(mapping.getViewId());
            
            int paramsLength = (params != null && params.length > 0) ? params.length/2 : 0;
            
            if(paramsLength > 0) {
                    
                    url.append("?");
                    
                    for(int paramIndex = 1; paramIndex < params.length; paramIndex = paramIndex + 2) {
                            
                            url.append(params[paramIndex - 1]);
                            url.append("=");
                            url.append(params[paramIndex]);
                            
                            if(paramIndex < paramsLength) {
                                    url.append("&");
                            }
                            
                    }
            
            }
            return externalContext.encodeResourceURL(url.toString());
            
    }
}
