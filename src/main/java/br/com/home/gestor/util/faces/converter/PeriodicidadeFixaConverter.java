package br.com.home.gestor.util.faces.converter;


import br.com.home.gestor.modelo.PeriodicidadeFixa;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "periodicidadeFixaConverter")
public class PeriodicidadeFixaConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {

        if(s == null){
            return null;
        }
        return PeriodicidadeFixa.valueOf(s);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {

        if(o == null){
            return null;
        }
        if(o instanceof PeriodicidadeFixa){
            return ((PeriodicidadeFixa)o).name();
        }else{
            throw new ConverterException(new FacesMessage(String.format("%s não é do enum", PeriodicidadeFixa.class)));
        }
    }
}
