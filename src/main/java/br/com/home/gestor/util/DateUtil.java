package br.com.home.gestor.util;

import java.util.Date;

import org.joda.time.LocalDate;

public class DateUtil {

	public static Date getDataInicial(int mes, int ano){
		return new LocalDate(ano, mes, 1).toDate();
	}
	
	public static Date getDataFinal(int mes, int ano){
		return new LocalDate(ano, mes, 1).dayOfMonth().withMaximumValue().toDate();
	}
	
	public static final Date getNewDate(final Date data){
		if(data == null){
			return null;
		}
		return new Date(data.getTime());
	}
}
