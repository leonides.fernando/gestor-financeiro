package br.com.home.gestor.util.faces.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import org.springframework.web.jsf.FacesContextUtils;

import br.com.home.gestor.util.faces.support.ViewScopedEntitySupport;

@FacesConverter(value = "viewEntityConverter")
public class ViewScopedEntityConverter implements Converter {
        
        private static final String ENTITY_CLASS = "viewEntityConverter.entityClass";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String entityId) {
                
                ViewScopedEntitySupport viewScopedEntitySupport = getEntitySupport(facesContext);
                
                if(viewScopedEntitySupport == null) {
                        throw new ConverterException(new FacesMessage("ViewScopedEntitySupport não foi definido"));
                }
                
                Class<?> entityClass = (Class<?>) component.getAttributes().get(ENTITY_CLASS);
                
                try {
                        
                        return viewScopedEntitySupport.getAsObject(entityId, entityClass);
                        
                } catch(IllegalStateException illegalStateException) {
                        
                        throw new ConverterException(new FacesMessage(illegalStateException.getMessage()));
                        
                }
                
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object entityValue) {
                
                ViewScopedEntitySupport viewScopedEntitySupport = getEntitySupport(facesContext);
                
                if(viewScopedEntitySupport == null) {
                        throw new ConverterException(new FacesMessage("ViewScopedEntitySupport não foi definido"));
                }
                
                if(entityValue == null) {
                        return null;
                }
                
                component.getAttributes().put(ENTITY_CLASS, entityValue.getClass());

                try {
                        
                        return viewScopedEntitySupport.getAsString(entityValue);
                        
                } catch(IllegalStateException illegalStateException) {
                        
                        throw new ConverterException(new FacesMessage(illegalStateException.getMessage()));
                        
                }
                
        }
        
        public ViewScopedEntitySupport getEntitySupport(FacesContext facesContext) {
                
                return (ViewScopedEntitySupport) FacesContextUtils
                                .getRequiredWebApplicationContext(facesContext)
                                .getBean("viewScopedEntitySupport");

        }

}