package br.com.home.gestor.util.faces.support;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;

@Named
@Scope("view")
public class ViewScopedEntitySupport implements Serializable {

        private static final long serialVersionUID = 6775096238528156889L;

        private static final String TRANSIENT_PREFIX = "#";
        
        @PersistenceContext
        private EntityManager entityManager;
        
        private Map<Object, String> transientEntities = new HashMap<Object, String>();
        
        public Object getAsObject(String entityId, Class<?> entityClass) {
                
                if(StringUtils.isBlank(entityId) || entityClass == null) {
                        
                        return null;
                        
                }
                
                // entityId refere-se a entidade transiente
                if(entityId.startsWith(TRANSIENT_PREFIX)) {

                        for(Entry<Object, String> entry : transientEntities.entrySet()) {
                                
                                if(entry.getValue().equals(entityId)) {
                                        
                                        return entry.getKey();
                                        
                                }
                                
                        }
                        
                }
                
                try {
                        
                        return entityManager.find(entityClass, Long.parseLong(entityId));
                        
                } catch(NumberFormatException numberFormatException) {
                        
                        // entityId não é Long
                        return null;
                        
                } catch(IllegalArgumentException illegalArgumentException) {
                        
                        throw new IllegalStateException(String.format("%s não denota uma entidade", entityClass.getName()));
                        
                }
                
        }
        
        public String getAsString(Object entity) {
                
                try {
                        
                        Method getId = entity.getClass().getMethod("getId", (Class<?>[]) null);
                        
                        Object id = getId.invoke(entity, (Object[]) null);
                        
                        // entidade transiente
                        if(id == null) {
                                
                                if(!transientEntities.containsKey(entity)) {
                                        
                                        String uuid = "#" + UUID.randomUUID().toString();
                                        
                                        transientEntities.put(entity, uuid);
                                        
                                        return uuid;
                                        
                                } else {
                                        
                                        return transientEntities.get(entity);
                                        
                                }
                                                
                        }
                        
                        return id.toString();

                } catch (NoSuchMethodException noSuchMethodException) {
                        
                        throw new IllegalStateException(String.format("Não foi possível obter método getId da entidade %s", entity.getClass().getName()));
                        
                } catch (InvocationTargetException invocationTargetException) {

                        throw new IllegalStateException(String.format("A invocação método getId() da entidade %s lançou uma exceção", entity.getClass().getName()));

                } catch (IllegalAccessException illegalAccessException) {
                        
                        throw new IllegalStateException(String.format("Não foi possível acessar método getId da entidade %s", entity.getClass().getName()));
                        
                }
        }
        
}