package br.com.home.gestor.controller;

import br.com.home.gestor.modelo.Categoria;
import br.com.home.gestor.modelo.Lancamento;
import br.com.home.gestor.modelo.LancamentoFixo;
import br.com.home.gestor.service.LancamentoFixoService;
import br.com.home.gestor.service.LancamentoService;
import br.com.home.gestor.util.faces.JsfUtil;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Named
@Scope("view")
public class LancamentoController implements Serializable{

	@Inject
	private LancamentoService lancamentoService;

	@Inject
    private LancamentoFixoService lancamentoFixoService;

	private Lancamento lancamento;
	
	private int numeroParcelas;
	
	private TipoParcelamento tipoParcelamentoSelecionado;
	
	private boolean repeticao;
	
	private boolean alterarEsteEPosteriores;
	
	private boolean alterarTodosLancamentos;
	
	private Categoria categoria;
	
	private Categoria categoriaARemover;

    private Long lancamentoId;
	
	public enum TipoParcelamento{
		FIXO("é um lançamento fixo"),
		PARCELADO("é um lançamento parcelado");
		
		private TipoParcelamento(String tipo){
			this.tipo = tipo;
		}
		
		private final String tipo;
		
		public String getTipo() {
			return tipo;
		}
	}

	public void init(){
		novoLancamento();

		if(lancamentoId != null){
			lancamento = lancamentoService.findById(lancamentoId);
			if(lancamento.getPeriodicidadeParcelada() != null){
				tipoParcelamentoSelecionado = TipoParcelamento.PARCELADO;
				numeroParcelas = lancamento.getNumeroParcela();
//				falta tratar como a edicao de um lancamento parcelado
				// deve editar apenas os lancamentos que ainda estão com data maior a atual, e colocar um aviso, tentar usar o alert do bootstrap
			}
			categoria = new ArrayList<Categoria>(lancamento.getCategorias()).get(0);
			repeticao = (lancamento.getPeriodicidadeFixa() != null) || (lancamento.getPeriodicidadeParcelada() != null) && isMostraRepetir();
		}
	}
		
	public void novoLancamento(){
		lancamento = Lancamento.novo();
		tipoParcelamentoSelecionado = null;
		categoria = null;
		repeticao = false;
	}
	
	public void cancelar(){
		novoLancamento();
	}
	
	public void adicionaCategoria(){
		if(categoria != null){
			lancamento.adicionaCategoria(categoria);
			categoria = null;
		}
	}
	
	public void removeCategoria(){
			lancamento.removeCategoria(categoriaARemover);
			categoriaARemover = null;
	}
	
	protected boolean isValido(){
		boolean resultado = true;
		
		if(lancamento.getTipoLancamento() == null){
			JsfUtil.addWarMessage("Informe o tipo de lançamento(entrada/saída)");
			resultado = false;
		}
		
		if(!StringUtils.hasText(lancamento.getDescricao())){
			JsfUtil.addWarMessage("A descrição deve ser informada");
			resultado = false;
		}
		
		if(lancamento.getValor() == null || BigDecimal.ZERO.equals(lancamento.getValor())){
			JsfUtil.addWarMessage("O valor deve ser informado");
			resultado = false;
		}
		
		if(lancamento.getDataLancamento() == null){
			JsfUtil.addWarMessage("A data do lançamento deve ser informada");
			resultado = false;
		}
		if(lancamento.getCategorias().isEmpty()){
			JsfUtil.addWarMessage("Ao menos uma categoria deve ser informada");
			resultado = false;
		}
		
		if(lancamento.getTipoPagamento() == null){
			JsfUtil.addWarMessage("O modo de pagamento deve ser informado");
			resultado = false;
		}
		
		return resultado;
	}
	
	public void salvar(){

		if(!isValido()){
			return;
		}
		if(!repeticao){
			lancamento.setPeriodicidadeFixa(null);
			lancamento.setPeriodicidadeParcelada(null);
		}
		
		if(tipoParcelamentoSelecionado != null && isRepeticao()){
			if(TipoParcelamento.FIXO.equals(tipoParcelamentoSelecionado)){
                lancamentoFixoService.save(LancamentoFixo.fromLancamento(lancamento));

			}else if(TipoParcelamento.PARCELADO.equals(tipoParcelamentoSelecionado)){
				int parcelaInicial = 1;
				if(!isNovo(lancamento) && alterarEsteEPosteriores){
					parcelaInicial = lancamento.getNumeroParcela();
				}
				lancamentoService.registraLancamentoParcelado(lancamento, parcelaInicial, numeroParcelas);
			}
		}else{
			lancamentoService.registraLancamento(lancamento);
		}
		JsfUtil.addInfoMessage("Lançamento salvo");
		novoLancamento();
	}
	
	public Lancamento getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}
	
	private boolean isNovo(Lancamento lancamento){
		return lancamento.getId() != null;
	}


	public int getNumeroParcelas() {
		return numeroParcelas;
	}

	public void setNumeroParcelas(int numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}
	
	public List<TipoParcelamento> getTiposParcelamento(){
		return Arrays.asList(TipoParcelamento.values());
	}

	public boolean isRepeticao() {
		return repeticao;
	}

	public void setRepeticao(boolean repeticao) {
		this.repeticao = repeticao;
	}
	
	public boolean isMostraRepetir(){
		return lancamento.getId() == null && !TipoParcelamento.PARCELADO.equals(tipoParcelamentoSelecionado);
	}
	
	public boolean isEdicaoLancamentoParcelado(){
		return lancamento.getId() != null && TipoParcelamento.PARCELADO.equals(tipoParcelamentoSelecionado);
	}
	
	public boolean isMostraPanelRepeticaoFixo(){
		return isMostraRepetir() && isRepeticao() && TipoParcelamento.FIXO.equals(tipoParcelamentoSelecionado);
	}
	
	public boolean isMostraPanelRepeticaoParcelado(){
		return isRepeticao() && TipoParcelamento.PARCELADO.equals(tipoParcelamentoSelecionado);
	}

	public TipoParcelamento getTipoParcelamentoSelecionado() {
		return tipoParcelamentoSelecionado;
	}

	public void setTipoParcelamentoSelecionado(
			TipoParcelamento tipoParcelamentoSelecionado) {
		this.tipoParcelamentoSelecionado = tipoParcelamentoSelecionado;
	}

	public boolean isAlterarEsteEPosteriores() {
		return alterarEsteEPosteriores;
	}

	public void setAlterarEsteEPosteriores(boolean alterarEsteEPosteriores) {
		this.alterarEsteEPosteriores = alterarEsteEPosteriores;
	}

	public boolean isAlterarTodosLancamentos() {
		return alterarTodosLancamentos;
	}

	public void setAlterarTodosLancamentos(boolean alterarTodosLancamentos) {
		this.alterarTodosLancamentos = alterarTodosLancamentos;
	}
	
	public void ajustaAlteracaoSeguintes(AjaxBehaviorEvent event){

		if(alterarEsteEPosteriores){
			alterarTodosLancamentos = false;
		}
	}

	public void ajustaAlteracaoTodos(AjaxBehaviorEvent event){
		if(alterarTodosLancamentos){
			alterarEsteEPosteriores = false;
		}
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Categoria getCategoriaARemover() {
		return categoriaARemover;
	}

	public void setCategoriaARemover(Categoria categoriaARemover) {
		this.categoriaARemover = categoriaARemover;
	}

    public Long getLancamentoId() {
        return lancamentoId;
    }

    public void setLancamentoId(Long lancamentoId) {
        this.lancamentoId = lancamentoId;
    }
}