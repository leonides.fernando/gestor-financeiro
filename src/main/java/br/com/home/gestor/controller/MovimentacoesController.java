package br.com.home.gestor.controller;

import br.com.home.gestor.dh.ItemFiltroCategoria;
import br.com.home.gestor.dh.ItemFiltroModoPagamento;
import br.com.home.gestor.modelo.Categoria;
import br.com.home.gestor.modelo.Lancamento;
import br.com.home.gestor.modelo.Mes;
import br.com.home.gestor.modelo.TipoPagamento;
import br.com.home.gestor.service.CategoriaService;
import br.com.home.gestor.service.LancamentoService;
import br.com.home.gestor.service.TipoPagamentoService;
import br.com.home.gestor.util.DateUtil;
import br.com.home.gestor.util.faces.JsfUtil;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDate;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static br.com.home.gestor.util.Constantes.BR;

@Named
@Scope("view")
public class MovimentacoesController implements Serializable{

	@Inject
	private LancamentoService lancamentoService;
	
	@Inject
	private TipoPagamentoService tipoPagamentoService;
	
	private Date data;
	
	private List<Lancamento> lancamentos;
	
	private BigDecimal totalReceitas;
	
	private BigDecimal totalDespezas;
	
	private BigDecimal saldoAnterior;
	
	private BigDecimal totalReceitaParcial = BigDecimal.ZERO;
	
	private BigDecimal totalDespezaParcial = BigDecimal.ZERO;
	
	private boolean mostraConfirmarExclusao;
	
	private ItemFiltroCategoria itemFiltroCategoria;
	
	private ItemFiltroModoPagamento itemFiltroModoPagamento;
	
	private List<ItemFiltroCategoria> categorias = Lists.newArrayList();
	
	private List<ItemFiltroModoPagamento> modosPagamento = Lists.newArrayList();
	
	@Inject
	private CategoriaService categoriaService;
	
	private List<Categoria> categoriasSelecionadas = Lists.newArrayList();
	
	private List<TipoPagamento> modosPagamentoSelecionados = Lists.newArrayList();
	
	
	private Mes mes;
	private int ano;
	
	
	private static final Predicate<ItemFiltroModoPagamento> MODOS_PAGAMENTO_SELECIONADOS_PREDICATE = new Predicate<ItemFiltroModoPagamento>() {

		@Override
		public boolean apply(ItemFiltroModoPagamento item) {
			return item.isSelecionado();
		}
	};
	
	private static final Function<ItemFiltroModoPagamento, TipoPagamento> MODOS_PAGAMENTO_SELECIONADOS_FUNCTION = new Function<ItemFiltroModoPagamento, TipoPagamento>() {

		@Override
		public TipoPagamento apply(ItemFiltroModoPagamento item) {
			return item.getTipoPagamento();
		}
	};
	
	
	private static final Predicate<ItemFiltroCategoria> CATEGORIAS_SELECIONADAS_PREDICATE = new Predicate<ItemFiltroCategoria>() {

		@Override
		public boolean apply(ItemFiltroCategoria item) {
			return item.isSelecionado();
		}
	};
	
	
	private static final Function<ItemFiltroCategoria, Categoria> CATEGORIAS_SELECIONADAS_FUNCTION = new Function<ItemFiltroCategoria, Categoria>() {

		@Override
		public Categoria apply(ItemFiltroCategoria item) {
			return item.getCategoria();
		}
	};
	
	@PostConstruct
	public void init(){
		
		LocalDate hoje = LocalDate.now();
		data = hoje.toDate();
		ano = hoje.getYear();
		List<Categoria> list = categoriaService.findAll();
		for (Categoria categoria : list) {
			ItemFiltroCategoria item = new ItemFiltroCategoria(categoria);
			categorias.add(item);
		}
		
		Collections.sort(categorias, new Comparator<ItemFiltroCategoria>() {

			@Override
			public int compare(ItemFiltroCategoria i1, ItemFiltroCategoria i2) {
				return i1.getCategoria().getNome().compareToIgnoreCase(i2.getCategoria().getNome());
			}
		});
		
		List<TipoPagamento> listaTiposPagamento = tipoPagamentoService.findAll();
		for (TipoPagamento tipoPagamento : listaTiposPagamento) {
			modosPagamento.add(new ItemFiltroModoPagamento(tipoPagamento));
		}
		Collections.sort(modosPagamento, new Comparator<ItemFiltroModoPagamento>() {

			@Override
			public int compare(ItemFiltroModoPagamento o1,
					ItemFiltroModoPagamento o2) {
				return o1.getText().compareToIgnoreCase(o2.getText());
			}
		});
		
		mes = new Mes(hoje.getMonthOfYear());
		
		buscar();
	}
	
	public void filtrar(){
		data = new LocalDate(ano, mes.getId(), 1).toDate();
		buscar();
	}
	
	public void resetaBusca(){
		data = LocalDate.now().toDate();
		buscar();
	}
	
	public void buscar(){
		lancamentos = lancamentoService.findLancamentosNoMes(data);
		calculaReceitasDespezas();
		zerarTotaisParciais();

		filtrarPorCategoriasSelecionadas();
		filtrarPorModosPagamentoSelecionados();
		
	}

	private void filtrarPorModosPagamentoSelecionados() {
		if(!CollectionUtils.isEmpty(modosPagamentoSelecionados)){
			final List<Lancamento> list = FluentIterable.from(lancamentos).filter(new LancamentosPorModoPagamentoPredicate(modosPagamentoSelecionados)).toList();
			lancamentos = new ArrayList<>(list);
			calculaTotaisParciais();
		}
	}

	private void filtrarPorCategoriasSelecionadas() {
		if(categoriasSelecionadas != null && !categoriasSelecionadas.isEmpty()){

			Predicate<Lancamento> lancamentosCategorizadosPredicate = new LancamentosPorCategoriaPredicate(categoriasSelecionadas);
			
			List<Lancamento> list = Lists.newArrayList(Iterables.filter(lancamentos, lancamentosCategorizadosPredicate));
			lancamentos = new ArrayList<>(list);
			calculaTotaisParciais();
		}
	}
	
	private void zerarTotaisParciais(){
		totalDespezaParcial = totalReceitaParcial = BigDecimal.ZERO;
	}
	
	private void calculaTotaisParciais() {
		
		for (Lancamento lancamento : lancamentos) {
			if(lancamento.isEntrada()){
				totalReceitaParcial = totalReceitaParcial.add(lancamento.getValor());
			}else{
				totalDespezaParcial = totalDespezaParcial.add(lancamento.getValor());
			}
		}
	}

	public void adicionarReceita(){
		JsfUtil.redirect(JsfUtil.prettyURL(Redirect.LANCAMENTO_ENTRADA));
	}
	
	public void addOuRemoveItemFiltroCategoria(){
		itemFiltroCategoria.setSelecionado(!itemFiltroCategoria.isSelecionado());
		categoriasSelecionadas = Lists.newArrayList(
				Iterables.transform(Lists.newArrayList(Iterables.filter(categorias, CATEGORIAS_SELECIONADAS_PREDICATE)),
				CATEGORIAS_SELECIONADAS_FUNCTION));
		
		buscar();
	}
	
	public void limparFiltroCategorias(){
		categoriasSelecionadas = Lists.newArrayList();
		for (ItemFiltroCategoria item : categorias) {
			item.setSelecionado(false);
		}
		buscar();
	}
	
	public void addOuRemoveItemFiltroModoPagamento(){
		itemFiltroModoPagamento.setSelecionado(!itemFiltroModoPagamento.isSelecionado());
		modosPagamentoSelecionados = FluentIterable.from(modosPagamento)
					.filter(MODOS_PAGAMENTO_SELECIONADOS_PREDICATE)
					.transform(MODOS_PAGAMENTO_SELECIONADOS_FUNCTION).toList();
		buscar();
	}
	
	public void limparFiltroModoPagamento(){
		modosPagamentoSelecionados = Lists.newArrayList();
		for (ItemFiltroModoPagamento item : modosPagamento) {
			item.setSelecionado(false);
		}
		buscar();
	}
	
	public void editar(Lancamento lancamento){
		
		JsfUtil.redirect(JsfUtil.prettyURL(Redirect.LANCAMENTO,"lancamentoId", lancamento.getId() ));
	}
	
	public void remover(Lancamento lancamento){
		lancamentos.remove(lancamento);
		lancamentoService.remove(lancamento.getId());
		zerarTotaisParciais();
		filtrarPorCategoriasSelecionadas();
		filtrarPorModosPagamentoSelecionados();
		JsfUtil.addInfoMessage("Lançamento removido com sucesso");
	}
	
	public void pago(Lancamento lancamento){
		lancamento.setPago(true);
		lancamentoService.save(lancamento);
	}
	
	public void naoPago(Lancamento lancamento){
		lancamento.setPago(false);
		lancamentoService.save(lancamento);
	}

	private void calculaReceitasDespezas() {
		
		LocalDate dataBase = LocalDate.fromDateFields(data);
		int mes = dataBase.getMonthOfYear();
		int ano = dataBase.getYear();
		totalReceitas = lancamentoService.calculaTotalEntradasMes(DateUtil.getDataInicial(mes, ano), 
				DateUtil.getDataFinal(mes, ano));
		totalDespezas = lancamentoService.calculaTotalSaidasMes(DateUtil.getDataInicial(mes, ano), 
				DateUtil.getDataFinal(mes, ano));

		saldoAnterior = lancamentoService.calculaSaldoMesAnterior(data);
		totalReceitaParcial = totalReceitas;
		totalDespezaParcial = totalDespezas;
	}

	public Date getData() {
		return DateUtil.getNewDate(data);
	}

	public void setData(Date data) {
		this.data = data;
	}

	public List<Lancamento> getLancamentos() {
		return lancamentos;
	}
	
	public void voltaMes(){
		data = LocalDate.fromDateFields(data).minusMonths(1).toDateTimeAtStartOfDay().toDate();
		buscar();
	}
	
	public void avancaMes(){
		data = LocalDate.fromDateFields(data).plusMonths(1).toDateTimeAtStartOfDay().toDate();
		buscar();
	}
	
	public String getMesStr(){
		DateFormat df = new SimpleDateFormat("MMMM 'de' yyyy", BR);
		return df.format(data);
	}
	
	public BigDecimal getTotalDespezas() {
		return totalDespezas;
	}
	
	public BigDecimal getTotalReceitas() {
		return totalReceitas;
	}
	
	public BigDecimal getSaldo(){
		return getTotalReceitas().add(getSaldoAnterior()) .subtract(getTotalDespezas());
	}

	public boolean isMostraConfirmarExclusao() {
		return mostraConfirmarExclusao;
	}

	public void setMostraConfirmarExclusao(boolean mostraConfirmarExclusao) {
		this.mostraConfirmarExclusao = mostraConfirmarExclusao;
	}

	public ItemFiltroCategoria getItemFiltroCategoria() {
		return itemFiltroCategoria;
	}

	public void setItemFiltroCategoria(ItemFiltroCategoria itemFiltro) {
		this.itemFiltroCategoria = itemFiltro;
	}
	
	public List<ItemFiltroCategoria> getCategorias() {
		return categorias;
	}
	
	public void setMes(int id){
		mes = new Mes(id);
	}
	
	public int getMes(){
		return mes.getId();
	}
	
	public int getAno() {
		return ano;
	}
	
	public void setAno(int ano) {
		this.ano = ano;
	}
	
	public BigDecimal getSaldoAnterior() {
		return saldoAnterior;
	}
	
	public BigDecimal getTotalDespezaParcial() {
		return totalDespezaParcial;
	}
	
	public BigDecimal getTotalReceitaParcial() {
		return totalReceitaParcial;
	}
	
	public List<ItemFiltroModoPagamento> getModosPagamento() {
		return modosPagamento;
	}
	
	public void setItemFiltroModoPagamento(
			ItemFiltroModoPagamento itemFiltroModoPagamento) {
		this.itemFiltroModoPagamento = itemFiltroModoPagamento;
	}
	
	public ItemFiltroModoPagamento getItemFiltroModoPagamento() {
		return itemFiltroModoPagamento;
	}
	
	public boolean isTemTotalParcial(){
		return (totalDespezaParcial.compareTo(BigDecimal.ZERO) != 0) ||
				(totalReceitaParcial.compareTo(BigDecimal.ZERO) != 0);
	}
	
	private static class LancamentosPorCategoriaPredicate implements Predicate<Lancamento> {

		private final List<Categoria> categorias;

		public LancamentosPorCategoriaPredicate(List<Categoria> categorias){
			this.categorias = categorias;
		}
		
		@Override
		public boolean apply(Lancamento lancamento) {
			
			for (final Categoria categoria : lancamento.getCategorias()) {
				if(categorias.contains(categoria)){
					return true;
				}
			}
			return false;
		}
	}
	
	private static class LancamentosPorModoPagamentoPredicate implements Predicate<Lancamento>{

		private final List<TipoPagamento> modosPagamento;
		
		public LancamentosPorModoPagamentoPredicate(final List<TipoPagamento> modosPagamento){
			this.modosPagamento = modosPagamento;
		}
		
		@Override
		public boolean apply(Lancamento lancamento) {
			if(modosPagamento.contains(lancamento.getTipoPagamento())){
				return true;
			}
			return false;
		}
	}
}
