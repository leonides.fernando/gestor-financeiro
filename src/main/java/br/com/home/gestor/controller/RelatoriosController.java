package br.com.home.gestor.controller;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.joda.time.LocalDate;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.context.annotation.Scope;

import br.com.home.gestor.dh.LancamentoCategoria;
import br.com.home.gestor.modelo.Mes;

@Named
@Scope("view")
public class RelatoriosController implements Serializable{

	@Inject
	private GraficoPizzaPorCategoriaController graficoPizzaPorCategoriaController;
	
	@Inject
	private TabelaEntradaSaidaPorCategoriaController tabelaEntradaSaidaPorCategoriaController;
	
	private boolean mostraGrafico;
	
	private Mes mes;
	private int ano;
	
	public void init(){
		LocalDate hoje = LocalDate.now();
		
		mes = new Mes(hoje.getMonthOfYear());
		ano = hoje.getYear();
		mostraGrafico = true;
		atualizar();
	}
	
	public void atualizar(){
		graficoPizzaPorCategoriaController.createChart(mes.getId(), ano);
		tabelaEntradaSaidaPorCategoriaController.createTable(mes.getId(), ano);
	}
	
	public PieChartModel getGraficoPizzaPorCategoriaChart() {
		return graficoPizzaPorCategoriaController.getChartModel();
	}
	
	public List<LancamentoCategoria> getTabelaEntradaSaidaCategoria(){
		return tabelaEntradaSaidaPorCategoriaController.getLancamentosPorPeriodo();
	}

	public int getAno() {
		return ano;
	}
	
	public void setMes(int id){
		mes = new Mes(id);
	}
	
	public int getMes(){
		return mes.getId();
	}
	
	public boolean isMostraGrafico() {
		return mostraGrafico;
	}
	
	public void trocaGraficoTabela(){
		mostraGrafico = !mostraGrafico;
	}
}
