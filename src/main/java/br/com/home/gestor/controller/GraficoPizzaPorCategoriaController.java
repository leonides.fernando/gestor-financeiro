package br.com.home.gestor.controller;

import br.com.home.gestor.dh.LancamentoCategoria;
import br.com.home.gestor.service.LancamentoService;
import br.com.home.gestor.util.DateUtil;
import com.google.common.base.Joiner;
import org.primefaces.model.chart.LegendPlacement;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

@Named
@Scope("request")
public class GraficoPizzaPorCategoriaController {

	@Inject
	private LancamentoService lancamentoService;
	
	private PieChartModel chartModel;
	
	public void createChart(int mes, int ano){
		
		Date dataInicial = DateUtil.getDataInicial(mes, ano);
		Date dataFinal = DateUtil.getDataFinal(mes, ano);
		
		chartModel = new PieChartModel();
		chartModel.setTitle("Fluxo Mensal");
		chartModel.setShowDataLabels(true);
		chartModel.setLegendPosition("w");
		chartModel.setLegendPlacement(LegendPlacement.OUTSIDEGRID);
		chartModel.setShowDatatip(true);
		chartModel.setMouseoverHighlight(true);
		chartModel.setShadow(true);
		chartModel.setSliceMargin(2);

		List<LancamentoCategoria> lancamentosPorPeriodo = lancamentoService.findLancamentosPorPeriodo(dataInicial, dataFinal);
		for (LancamentoCategoria lc : lancamentosPorPeriodo) {
			chartModel.set(Joiner.on('/').join(lc.getTipoLancamento().getTipo(), lc.getCategoria()), lc.getValor());
		}
	}
	
	public PieChartModel getChartModel() {
		return chartModel;
	}
}
