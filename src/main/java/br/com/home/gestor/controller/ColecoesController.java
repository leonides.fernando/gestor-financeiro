package br.com.home.gestor.controller;

import static br.com.home.gestor.util.Constantes.BR;

import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import br.com.home.gestor.modelo.Categoria;
import br.com.home.gestor.modelo.Mes;
import br.com.home.gestor.modelo.PeriodicidadeFixa;
import br.com.home.gestor.modelo.PeriodicidadeParcelada;
import br.com.home.gestor.modelo.StatusItemDesejado;
import br.com.home.gestor.modelo.TipoLancamento;
import br.com.home.gestor.modelo.TipoPagamento;
import br.com.home.gestor.service.CategoriaService;
import br.com.home.gestor.service.TipoPagamentoService;

import com.google.common.collect.Lists;

@Named
@Scope("view")
public class ColecoesController implements Serializable{

	@Inject
	private CategoriaService categoriaService;
	
	@Inject
	private TipoPagamentoService tipoPagamentoService;
	
	private List<Categoria> categorias;
	
	private List<TipoPagamento> tiposPagamento;
	
	private List<Integer> numeroParcelas;
	
	private List<Mes> meses = Lists.newArrayList();
	
	private List<StatusItemDesejado> statusItensDesejados = Arrays.asList(StatusItemDesejado.values());
	
	
	@PostConstruct
	protected void init(){
		numeroParcelas = new ArrayList<Integer>();	
		for(int i = 2; i <= 20; i++){
			numeroParcelas.add(i);
		}
		
		String[] arrayMeses = new DateFormatSymbols(BR).getShortMonths();
		for(int i = 0; i < arrayMeses.length-1; i++){
			meses.add(new Mes(i+1, arrayMeses[i]));
		}
	}
	
	
	public List<TipoPagamento> getTiposPagamento() {
		if(tiposPagamento == null){
			tiposPagamento = tipoPagamentoService.findAll();
		}
		return tiposPagamento;
	}
	
	public List<Categoria> getCategorias() {
		
		if(categorias == null){
			categorias = categoriaService.findAll();
			
			Collections.sort(categorias, new Comparator<Categoria>() {

				@Override
				public int compare(Categoria c1, Categoria c2) {
					return c1.getNome().compareToIgnoreCase(c2.getNome());
				}
				
			});
		}
		
		return categorias;
	}
	
	public List<TipoLancamento> getTiposLancamento() {
		return Arrays.asList(TipoLancamento.values());
	}
	
	public List<PeriodicidadeFixa> getPeriodicidadeFixas(){
		return Arrays.asList(PeriodicidadeFixa.values());
	}
	
	public List<PeriodicidadeParcelada> getPeriodicidadeParceladas(){
		return Arrays.asList(PeriodicidadeParcelada.values());
	}
	
	public List<Integer> getNumeroParcelas() {
		return numeroParcelas;
	}
	
	public List<Mes> getMeses() {
		return meses;
	}
	
	public List<StatusItemDesejado> getStatusItensDesejados() {
		return statusItensDesejados;
	}
}
