package br.com.home.gestor.controller;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import br.com.home.gestor.dh.LancamentoCategoria;
import br.com.home.gestor.service.LancamentoService;
import br.com.home.gestor.util.DateUtil;

@Named
@Scope("request")
public class TabelaEntradaSaidaPorCategoriaController implements Serializable{

	@Inject
	private LancamentoService lancamentoService;
	
	private List<LancamentoCategoria> lancamentosPorPeriodo;
	
	public void createTable(int mes, int ano){
		
		Date dataInicial = DateUtil.getDataInicial(mes, ano);
		Date dataFinal = DateUtil.getDataFinal(mes, ano);
		
		lancamentosPorPeriodo = lancamentoService.findLancamentosPorPeriodo(dataInicial, dataFinal);
		
		Collections.sort(lancamentosPorPeriodo, new Comparator<LancamentoCategoria>() {

			@Override
			public int compare(LancamentoCategoria lc1, LancamentoCategoria lc2) {
				int resultadoTipoLancamento = lc1.getTipoLancamento().compareTo(lc2.getTipoLancamento());
				
				if(resultadoTipoLancamento == 0){
					return lc1.getCategoria().compareToIgnoreCase(lc2.getCategoria());
				}
				return resultadoTipoLancamento;
			}
		});
	}
	
	public List<LancamentoCategoria> getLancamentosPorPeriodo() {
		return lancamentosPorPeriodo;
	}
}
