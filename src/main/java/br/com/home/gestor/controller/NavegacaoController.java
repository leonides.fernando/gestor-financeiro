package br.com.home.gestor.controller;

import java.io.Serializable;

import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import br.com.home.gestor.util.faces.JsfUtil;

@Named
@Scope("view")
public class NavegacaoController implements Serializable{

	public void goHome(){
		JsfUtil.redirect(JsfUtil.prettyURL(Redirect.HOME));
	}
}
