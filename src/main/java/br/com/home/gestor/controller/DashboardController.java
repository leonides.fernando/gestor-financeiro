package br.com.home.gestor.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.joda.time.LocalDate;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

import br.com.home.gestor.dh.HistoricoGastos;
import br.com.home.gestor.modelo.ItemDesejo;
import br.com.home.gestor.modelo.Lancamento;
import br.com.home.gestor.modelo.StatusItemDesejado;
import br.com.home.gestor.service.ConfiguracaoService;
import br.com.home.gestor.service.ItemDesejoService;
import br.com.home.gestor.service.LancamentoService;
import br.com.home.gestor.util.faces.JsfUtil;

@Named
@Scope("view")
public class DashboardController implements Serializable{

	@Inject
	private LancamentoService lancamentoService;
	
	@Inject
	private ItemDesejoService itemDesejoService;
	
	@Inject
	private ConfiguracaoService configuracaoService;
	
	private List<Lancamento> contasAReceber;
	
	private List<Lancamento> contasAPagar;
	
	
	private List<ItemDesejo> itensDesejo;

	private ItemDesejo item;
	
	private boolean buscarTodosItens = false;
	
	private static final EnumSet<StatusItemDesejado> statusList = EnumSet.of(StatusItemDesejado.CANCELADO,StatusItemDesejado.EM_ESPERA);
	
	private List<HistoricoGastos> gastos;
	
	private BigDecimal totalGastos;
	
	private int mesesDeAnalise;
	
	public void init(){
		
		initContasAPagarReceber();
		
		initItensDesejo();
		
		initHistoricoGastos();
	}

	private void initHistoricoGastos(){
		mesesDeAnalise = configuracaoService.get().getQuantidadeMesesMediaGastos();
		gastos = lancamentoService.findGastosPorIntervaloMeses(mesesDeAnalise);
		
		totalGastos = BigDecimal.ZERO;
		for (HistoricoGastos gasto : gastos) {
			totalGastos = totalGastos.add(gasto.getValorMes());
		}
	}


	private void initItensDesejo() {
		novoItem();
		buscarItens();
	}


	private void initContasAPagarReceber() {
	//		FIXME: adicionar filtro na lista de contas a receber e a pagar. ANALIZAR ANTES SE FAZ SENTIDO
			
		LocalDate dataBase = LocalDate.now().dayOfMonth().withMinimumValue();
			
		Date dataInicial = dataBase.toDate();
		Date dataFinal = dataBase.dayOfMonth().withMaximumValue().toDate();
			
		contasAReceber = lancamentoService.findContasAReceber(dataInicial, dataFinal);
		contasAPagar = lancamentoService.findContasAPagar(dataInicial, dataFinal);
	}
	
	
	//********************* Controlando as contas a Pagar
	public List<Lancamento> getContasAReceber() {
		return contasAReceber;
	}
	
	public void pago(Lancamento lancamento){
		contasAPagar.remove(lancamento);
		lancamento.setPago(true);
		lancamentoService.save(lancamento);
	}
	
	public void recebido(Lancamento lancamento){
	
		contasAReceber.remove(lancamento);
		lancamento.setPago(true);
		lancamentoService.save(lancamento);
	}
	
	public List<Lancamento> getContasAPagar() {
		return contasAPagar;
	}
	// ************************ Fim Controle Contas a Pagar 

	
	
	
	
	
	
	// ######################### Controle itens de desejo
	
	private void novoItem(){
		item = new ItemDesejo();
		item.setDataCriacao(LocalDate.now().toDate());
	}
	
	public void buscarItens(){
		itensDesejo = buscarTodosItens ? itemDesejoService.findAll() : itemDesejoService.findItensDesejoByStatusIn(statusList);
	}
	
	private void ordenaItens(){
		Collections.sort(itensDesejo, new Comparator<ItemDesejo>() {

			@Override
			public int compare(ItemDesejo it1, ItemDesejo it2) {
				return it1.getDataCriacao().compareTo(it2.getDataCriacao());
			}
		});
	}
	
	private boolean isValido(){
		boolean resultado = true;
		if(!StringUtils.hasText(item.getNome())){
			JsfUtil.addWarMessage("Informe o nome do item desejado");
			resultado = false;
		}
		
		if(item.getStatus() == null){
			JsfUtil.addWarMessage("Informe o status do item desejado");
			resultado = false;
		}
		
		return resultado;
	}
	
	public void editar(Long id){
		item = itemDesejoService.findById(id);
	}
	
	public void cancelarItemDesejo(){
		novoItem();
	}
	
	public void adicionaItem(){
		
		if(isValido()){
			adicionaOuSubstitui();
			ordenaItens();
			novoItem();
			JsfUtil.addInfoMessage("Item de desejo salvo com sucesso :-)");
		}
	}
	
	private void adicionaOuSubstitui() {
		
		int index = itensDesejo.indexOf(item);
		if(index >= 0){
			itensDesejo.remove(index);
		}
		itensDesejo.add(itemDesejoService.save(item));
	}

	public void removeItem(ItemDesejo itemARemover){
		itemDesejoService.remove(itemARemover.getId());
		itensDesejo.remove(itemARemover);
		ordenaItens();
		JsfUtil.addInfoMessage("Item removido com sucesso (y)");
	}
	
	public ItemDesejo getItem() {
		return item;
	}

	public void setItem(ItemDesejo item) {
		this.item = item;
	}

	public List<ItemDesejo> getItensDesejo() {
		return itensDesejo;
	}
	
	public boolean isBuscarTodosItens() {
		return buscarTodosItens;
	}
	
	public void setBuscarTodosItens(boolean buscarTodosItens) {
		this.buscarTodosItens = buscarTodosItens;
	}
	// ################################## Fim Conrole itens de desejo
	
	
	// --------------------------------------- Controle historico gastos
	
	public List<HistoricoGastos> getGastos() {
		return gastos;
	}

	public BigDecimal getTotalGastos() {
		return totalGastos;
	}
	
	public int getMesesDeAnalise() {
		return mesesDeAnalise;
	}
	
	// --------------------------------------- Fim Controle historico gastos
	
	
}
