package br.com.home.gestor.controller;

public interface Redirect {

	String LANCAMENTO_ENTRADA = "pretty:lancamentos-entradas";
	
	String LANCAMENTO = "pretty:lancamento";

    String EDITAR_LANCAMENTO = "pretty:editar-lancamento";
    
    String HOME = "pretty:dashboard";
}
