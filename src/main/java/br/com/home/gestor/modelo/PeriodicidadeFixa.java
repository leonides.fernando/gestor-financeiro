package br.com.home.gestor.modelo;


import org.joda.time.LocalDate;

public enum PeriodicidadeFixa {

	MENSAL("Mensal") {
	@Override
	public boolean isCriaNovoLancamentoPorData(LocalDate data) {
        final LocalDate dataMensal = data.plusMonths(1);
		return !hoje().isBefore(dataMensal);
	}
},
	BIMESTRAL("Bimestral") {
		@Override
		public boolean isCriaNovoLancamentoPorData(LocalDate data) {
            final LocalDate dataMensal = data.plusMonths(2);
            return !hoje().isBefore(dataMensal);
		}
	},
	TRIMESTRAL("Trimestral") {
		@Override
		public boolean isCriaNovoLancamentoPorData(LocalDate data) {
            final LocalDate dataMensal = data.plusMonths(3);
            return !hoje().isBefore(dataMensal);
		}
	},
	SEMESTRAL("Semestral") {
		@Override
		public boolean isCriaNovoLancamentoPorData(LocalDate data) {
            final LocalDate dataMensal = data.plusMonths(6);
            return !hoje().isBefore(dataMensal);
		}
	},
	ANUAL("Anual") {
		@Override
		public boolean isCriaNovoLancamentoPorData(LocalDate data) {
            final LocalDate dataMensal = data.plusMonths(12);
            return !hoje().isBefore(dataMensal);
		}
	};


	private PeriodicidadeFixa(String descricao){
		this.descricao = descricao;
	}

	private final String descricao;

	private static LocalDate hoje(){
        return LocalDate.now();
    }
	public abstract boolean isCriaNovoLancamentoPorData(LocalDate data);

	public String getDescricao() {
		return descricao;
	}
}
