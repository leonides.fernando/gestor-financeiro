package br.com.home.gestor.modelo;

import java.util.Date;

import org.joda.time.LocalDate;

public enum PeriodicidadeParcelada {

	SEMANAS,
	QUINZENAS,
	MESES,
	BIMESTRES,
	TRIMESTRES,
	SEMESTRES,
	ANOS;
	
	public Date getProximaDataPorPeriodicidade(Date data, int numeroParcela){
		
		switch (this) {
		case SEMANAS:
			return LocalDate.fromDateFields(data).plusWeeks(numeroParcela).toDateMidnight().toDate();

		case ANOS:
			return LocalDate.fromDateFields(data).plusYears(numeroParcela).toDateMidnight().toDate();
			
		case MESES:
			return LocalDate.fromDateFields(data).plusMonths(numeroParcela).toDateMidnight().toDate();
			
		case BIMESTRES:
			return LocalDate.fromDateFields(data).plusMonths(2).toDateMidnight().toDate();
			
		case TRIMESTRES:
			return LocalDate.fromDateFields(data).plusMonths(3).toDateMidnight().toDate();
			
		case SEMESTRES:
			return LocalDate.fromDateFields(data).plusMonths(6).toDateMidnight().toDate();
			
		case QUINZENAS:
			return LocalDate.fromDateFields(data).plusDays(15).toDateMidnight().toDate();
			
		default:
			throw new IllegalArgumentException("Tipo de periocidicade parcelada não suportado: " + this);
		}
	}
	
}
