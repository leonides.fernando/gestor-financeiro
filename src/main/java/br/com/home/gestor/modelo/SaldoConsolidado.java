package br.com.home.gestor.modelo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="saldo_consolidado")
public class SaldoConsolidado {

	@Id
	@GeneratedValue(generator = "saldo_consolidado_seq")
	@SequenceGenerator(name = "saldo_consolidado_seq", sequenceName = "saldo_consolidado_seq", initialValue = 1)
	private Long id;
	
	@Column
	private int mes;
	
	@Column
	private int ano;
	
	@Column(precision=12, scale=6)
	private BigDecimal valor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ano;
		result = prime * result + mes;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SaldoConsolidado other = (SaldoConsolidado) obj;
		if (ano != other.ano)
			return false;
		if (mes != other.mes)
			return false;
		return true;
	}
}
