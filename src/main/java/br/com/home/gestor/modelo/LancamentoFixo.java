package br.com.home.gestor.modelo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.LocalDate;

import com.google.common.base.Objects;
import com.google.common.collect.Sets;

@NamedQueries(value = {
        @NamedQuery(name = "lancamentoFixo.findByAtivoAndPeriodicidadeFixa",
                query = "select lf from LancamentoFixo lf join fetch lf.categorias where lf.ativo = true and lf.periodicidadeFixa = :periodicidadeFixa")
})

@Entity
@Table(name = "lancamento_fixo")
public class LancamentoFixo {

	@Id
	@GeneratedValue(generator = "lancamento_fixo_seq")
	@SequenceGenerator(name = "lancamento_fixo_seq", sequenceName = "lancamento_fixo_seq", initialValue = 1)
	private Long id;

	@Column(name = "descricao")
	private String descricao;

	@Column(precision = 12, scale = 6)
	private BigDecimal valor;

	@ManyToOne
	@JoinColumn(name = "tipo_pagamento_id")
	private TipoPagamento tipoPagamento;

	@Enumerated(EnumType.STRING)
	@Column(length = 32, name = "tipo_lancamento")
	private TipoLancamento tipoLancamento;

	@Column(name = "ativo")
	private boolean ativo;

	@OneToMany
	@JoinTable(name = "lancamento_fixo_categoria", joinColumns = @JoinColumn(name = "lancamento_fixo_id"), inverseJoinColumns = @JoinColumn(name = "categoria_id"))
	private Set<Categoria> categorias = new HashSet<>();

	@Column(name = "mes_inicio")
	private int mesInicio;

	@Column(name = "ano_inicio")
	private int anoInicio;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio")
	private Date dataInicio;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_termino")
	private Date dataTerminio;

	@Enumerated(EnumType.STRING)
	@Column(name = "periodicidade_fixa",length=32)
	private PeriodicidadeFixa periodicidadeFixa;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_ultimo_lancamento")
	private Date dataUltimoLancamento;

	@Column(name = "parcelas")
	private int parcelas;

	public LancamentoFixo() {
	}

	private LancamentoFixo(String descricao, BigDecimal valor,
                           TipoPagamento tipoPagamento, TipoLancamento tipoLancamento,
                           List<Categoria> categorias, PeriodicidadeFixa periodicidadeFixa) {

		LocalDate hoje = LocalDate.now();
		this.descricao = descricao;
		this.valor = valor;
		this.tipoPagamento = tipoPagamento;
		this.tipoLancamento = tipoLancamento;
		this.ativo = true;
		this.categorias = Sets.newHashSet(categorias);
        this.periodicidadeFixa = periodicidadeFixa;

		anoInicio = hoje.getYear();
		mesInicio = hoje.getMonthOfYear();
		dataInicio = hoje.toDate();
        parcelas = 1;
		dataUltimoLancamento = hoje.toDate();
	}

	public static LancamentoFixo fromLancamento(final Lancamento lancamento) {
		return new LancamentoFixo(lancamento.getDescricao(),
				lancamento.getValor(), lancamento.getTipoPagamento(),
				lancamento.getTipoLancamento(),
				lancamento.getCategorias(), lancamento.getPeriodicidadeFixa());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public TipoLancamento getTipoLancamento() {
		return tipoLancamento;
	}

	public void setTipoLancamento(TipoLancamento tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}

	public Set<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(Set<Categoria> categorias) {
		this.categorias = categorias;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public int getMesInicio() {
		return mesInicio;
	}

	public int getAnoInicio() {
		return anoInicio;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public Date getDataTerminio() {
		return dataTerminio;
	}

	public PeriodicidadeFixa getPeriodicidadeFixa() {
		return periodicidadeFixa;
	}

	public Date getDataUltimoLancamento() {
		return dataUltimoLancamento;
	}

	public int getParcelas() {
		return parcelas;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id, descricao, valor, tipoPagamento,
				tipoLancamento, dataUltimoLancamento, parcelas);
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof LancamentoFixo) {
			LancamentoFixo that = (LancamentoFixo) object;
			return Objects.equal(this.id, that.id)
					&& Objects.equal(this.descricao, that.descricao)
					&& Objects.equal(this.valor, that.valor)
					&& Objects.equal(this.tipoPagamento, that.tipoPagamento)
					&& Objects.equal(this.dataUltimoLancamento, that.dataUltimoLancamento)
					&& Objects.equal(this.parcelas, that.parcelas)
					&& Objects.equal(this.tipoLancamento, that.tipoLancamento);
		}
		return false;
	}

    public void ultimoLancamentoHoje() {
        dataUltimoLancamento = LocalDate.now().toDate();
    }
}
