package br.com.home.gestor.modelo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.home.gestor.util.DateUtil;


@Entity
@Table(name="lancamento")
public class Lancamento {

	@Id
	@GeneratedValue(generator = "lancamento_seq")
	@SequenceGenerator(name = "lancamento_seq", sequenceName = "lancamento_seq", initialValue = 1)
	private Long id;
	
	@Column(length=64)
	private String descricao;
	
	@Column(precision=12, scale=6)
	private BigDecimal valor;
	
	@Temporal(TemporalType.DATE)
	@Column
	private Date dataLancamento;
	
	@ManyToOne
	private TipoPagamento tipoPagamento;
	
	@Enumerated(EnumType.STRING)
	@Column(length=32)
	private TipoLancamento tipoLancamento;

	@Enumerated(EnumType.STRING)
	@Column(length=32)
	private PeriodicidadeParcelada periodicidadeParcelada;
	
	@Column
	private int numeroParcela;
	
	@Enumerated(EnumType.STRING)
	@Column(length=32)
	private PeriodicidadeFixa periodicidadeFixa;

	 @OneToMany
     @JoinTable(name="lancamento_categoria", joinColumns=@JoinColumn(name="lancamento_id"), 
                     inverseJoinColumns=@JoinColumn(name="categoria_id"))
	private List<Categoria> categorias = new ArrayList<Categoria>();
	
	@Column
	private boolean pago;
	
	@Column(name="mes")
	private int mes;
	
	@Column(name="ano")
	private int ano;


	protected Lancamento(){
	}

	private Lancamento(final LancamentoFixo lancamentoFixo){
		this.categorias = new ArrayList<>(lancamentoFixo.getCategorias());
		this.ano = lancamentoFixo.getAnoInicio();
		this.dataLancamento = new Date();
		this.descricao = lancamentoFixo.getDescricao() + " (" + lancamentoFixo.getParcelas()+")";
		this.mes = lancamentoFixo.getMesInicio();
		this.numeroParcela = lancamentoFixo.getParcelas();
		this.valor = lancamentoFixo.getValor();
		this.tipoLancamento = lancamentoFixo.getTipoLancamento();
		this.tipoPagamento = lancamentoFixo.getTipoPagamento();
		this.periodicidadeFixa = lancamentoFixo.getPeriodicidadeFixa();
	}

	public static Lancamento novo(){
		return new Lancamento();
	}
	public static Lancamento fromLancamentoFixo(final LancamentoFixo lancamentoFixo){
		return new Lancamento(lancamentoFixo);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Date getDataLancamento() {
		return DateUtil.getNewDate(dataLancamento);
	}

	public void setDataLancamento(Date dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	public TipoLancamento getTipoLancamento() {
		return tipoLancamento;
	}

	public void setTipoLancamento(TipoLancamento tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		result = prime * result
				+ ((dataLancamento == null) ? 0 : dataLancamento.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result
				+ ((tipoLancamento == null) ? 0 : tipoLancamento.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lancamento other = (Lancamento) obj;

		if (dataLancamento == null) {
			if (other.dataLancamento != null)
				return false;
		} else if (!dataLancamento.equals(other.dataLancamento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (tipoLancamento != other.tipoLancamento)
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}

	public PeriodicidadeParcelada getPeriodicidadeParcelada() {
		return periodicidadeParcelada;
	}

	public void setPeriodicidadeParcelada(PeriodicidadeParcelada periodicidade) {
		this.periodicidadeParcelada = periodicidade;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public PeriodicidadeFixa getPeriodicidadeFixa() {
		return periodicidadeFixa;
	}

	public void setPeriodicidadeFixa(PeriodicidadeFixa periodicidadeFixa) {
		this.periodicidadeFixa = periodicidadeFixa;
	}

	public boolean isPago() {
		return pago;
	}

	public void setPago(boolean pago) {
		this.pago = pago;
	}

	public int getNumeroParcela() {
		return numeroParcela;
	}

	public void setNumeroParcela(int numeroParcela) {
		this.numeroParcela = numeroParcela;
	}

	public List<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}
	
	public void adicionaCategoria(Categoria categoria){
		categorias.add(categoria);
	}
	
	public void removeCategoria(Categoria categoria){
		categorias.remove(categoria);
	}
	
	public String getTodasCategorias(){
		StringBuilder builder = new StringBuilder();
		for(Categoria categoria : getCategorias()){
			builder.append(categoria.getNome() + " / ");
		}
		if(builder.length() > 0){
			int ultimoSeparador = builder.lastIndexOf(" / "); 
			builder.delete(ultimoSeparador, builder.length());
		}
		return builder.toString();
	}

	public int getMes() {
		return mes;
	}

	public int getAno() {
		return ano;
	}
	
	public void setMesAno(int mes,  int ano){
		this.mes = mes;
		this.ano = ano;
	}
	
	public boolean isEntrada(){
		return TipoLancamento.ENTRADA == this.getTipoLancamento();
	}
}
