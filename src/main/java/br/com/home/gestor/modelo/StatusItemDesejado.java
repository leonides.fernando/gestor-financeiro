package br.com.home.gestor.modelo;

public enum StatusItemDesejado {

	
	EM_ESPERA("Em espera"), ADQUIRIDO("Adquirido"), CANCELADO("Cancelado");
	
	private StatusItemDesejado(String label){
		this.label = label;
	}
	
	private final String label;
	
	public String getLabel() {
		return label;
	}
}
