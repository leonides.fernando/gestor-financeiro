package br.com.home.gestor.modelo;

public enum TipoLancamento {

	ENTRADA("Entrada"), SAIDA("Saída");
	
	private TipoLancamento(String tipo){
		this.tipo = tipo;
	}
	
	private String tipo;
	
	public String getTipo() {
		return tipo;
	}
}
