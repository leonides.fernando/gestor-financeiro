package br.com.home.gestor.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="configuracao")
public class Configuracao {

	@Id
	@GeneratedValue(generator = "configuracao_seq")
	@SequenceGenerator(name = "configuracao_seq", sequenceName = "configuracao_seq", initialValue = 1)
	private Long id;
	
	@Column(name="quantidade_meses_media_gastos")
	private int quantidadeMesesMediaGastos;
	
	public int getQuantidadeMesesMediaGastos() {
		return quantidadeMesesMediaGastos;
	}
}
