package br.com.home.gestor.modelo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.home.gestor.util.DateUtil;

@Entity
@Table(name="item_desejo")
public class ItemDesejo {

	@Id
	@GeneratedValue(generator = "item_desejo_seq")
	@SequenceGenerator(name = "item_desejo_seq", sequenceName = "item_desejo_seq", initialValue = 1)
	private Long id;
	
	@Column(length=128)
	private String nome;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date dataCriacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date dataAquisicao;

	@Enumerated(EnumType.STRING)
	@Column(length=64)
	private StatusItemDesejado status;
	
	@Column(precision=21, scale=6)
	private BigDecimal valor;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String itemDesejado) {
		this.nome = itemDesejado;
	}

	public Date getDataCriacao() {
		return DateUtil.getNewDate(dataCriacao);
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataAquisicao() {
		return DateUtil.getNewDate(dataAquisicao);
	}

	public void setDataAquisicao(Date dataAquisicao) {
		this.dataAquisicao = dataAquisicao;
	}

	public StatusItemDesejado getStatus() {
		return status;
	}

	public void setStatus(StatusItemDesejado status) {
		this.status = status;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valorItem) {
		this.valor = valorItem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemDesejo other = (ItemDesejo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


}
