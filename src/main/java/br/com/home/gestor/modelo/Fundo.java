package br.com.home.gestor.modelo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.home.gestor.util.DateUtil;

@Entity
@Table(name="fundo")
public class Fundo {

	@Id
	@GeneratedValue(generator = "fundo_seq")
	@SequenceGenerator(name = "fundo_seq", sequenceName = "fundo_seq", initialValue = 1)
	private Long id;
	
	@Column(length=128)
	private String nome;
	
	@Column(precision=12, scale=6)
	private BigDecimal valorAplicado;
	
	@Temporal(TemporalType.DATE)
	@Column
	private Date dataAplicacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getValorAplicado() {
		return valorAplicado;
	}

	public void setValorAplicado(BigDecimal valorAplicado) {
		this.valorAplicado = valorAplicado;
	}

	public Date getDataAplicacao() {
		return DateUtil.getNewDate(dataAplicacao);
	}

	public void setDataAplicacao(Date dataAplicacao) {
		this.dataAplicacao = dataAplicacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataAplicacao == null) ? 0 : dataAplicacao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((valorAplicado == null) ? 0 : valorAplicado.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fundo other = (Fundo) obj;
		if (dataAplicacao == null) {
			if (other.dataAplicacao != null)
				return false;
		} else if (!dataAplicacao.equals(other.dataAplicacao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (valorAplicado == null) {
			if (other.valorAplicado != null)
				return false;
		} else if (!valorAplicado.equals(other.valorAplicado))
			return false;
		return true;
	}
	
	
}
