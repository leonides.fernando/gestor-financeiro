package br.com.home.gestor.modelo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.home.gestor.util.DateUtil;

@Entity
@Table(name="extrato")
public class Extrato{

	@Id
	@GeneratedValue(generator = "extrato_seq")
	@SequenceGenerator(name = "extrato_seq", sequenceName = "extrato_seq", initialValue = 1)
	private Long id;
	
	@Column(precision=12, scale=6)
	private BigDecimal saldo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date data;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public Date getData() {
		return DateUtil.getNewDate(data);
	}

	public void setData(Date data) {
		this.data = data;
	}
}
