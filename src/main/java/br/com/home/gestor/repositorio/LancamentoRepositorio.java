package br.com.home.gestor.repositorio;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.home.gestor.dh.HistoricoGastos;
import br.com.home.gestor.dh.LancamentoCategoria;
import br.com.home.gestor.modelo.Lancamento;
import br.com.home.gestor.modelo.TipoLancamento;

public interface LancamentoRepositorio extends GenericRepository<Lancamento, Long>{

	
	List<Lancamento> findByDataLancamentoBetween(Date dataInicial, Date dataFinal);
	
	@Query(value="select l from Lancamento l where l.tipoLancamento = 'ENTRADA' and l.pago = false and l.dataLancamento between :dataInicial and :dataFinal")
	List<Lancamento> findContasAReceberBetween(@Param("dataInicial")Date dataInicial, @Param("dataFinal")Date dataFinal);

	@Query(value="select l from Lancamento l where l.tipoLancamento = 'SAIDA' and l.pago = false and l.dataLancamento between :dataInicial and :dataFinal")
	List<Lancamento> findContasAPagar(@Param("dataInicial")Date dataInicial, @Param("dataFinal")Date dataFinal);
	
	@Query(value="select sum(l.valor) from Lancamento l where l.dataLancamento between :dataInicial and :dataFinal and l.tipoLancamento = :tipoLancamento")
	BigDecimal calculaTotalPorTipoLancamento(@Param("dataInicial")Date dataInicial, @Param("dataFinal")Date dataFinal, @Param("tipoLancamento") TipoLancamento tipoLancamento);
	
	
	@Query(value="select new br.com.home.gestor.dh.LancamentoCategoria(sum(l.valor),l.tipoLancamento,categoria.nome) "
			+ " from Lancamento l inner join l.categorias categoria "
			+ " where l.dataLancamento between :dataInicial and :dataFinal "
			+ " group by l.tipoLancamento, categoria.nome")
	List<LancamentoCategoria> findLancamentosPorPeriodo (@Param("dataInicial")Date dataInicial, @Param("dataFinal")Date dataFinal);
	
	
	@Query(value="select new br.com.home.gestor.dh.HistoricoGastos(avg(l.valor), categoria.nome) "
			+ " from Lancamento l inner join l.categorias categoria "
			+ " where l.tipoLancamento = 'SAIDA' and "
			+ " (l.mes >= :mesInicial and l.ano >= :anoInicial) or (l.mes <= :mesFinal and l.ano <= :anoFinal)"
			+ " group by categoria.nome "
			+ " order by avg(l.valor) desc ")
	List<HistoricoGastos> findGastosPorMesAno(@Param("mesInicial")int mesInicial, @Param("anoInicial")int anoInicial, @Param("mesFinal")int mesFinal, @Param("anoFinal")int anoFinal);
}
