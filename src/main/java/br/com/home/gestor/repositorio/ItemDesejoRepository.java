package br.com.home.gestor.repositorio;

import java.util.List;
import java.util.Set;

import br.com.home.gestor.modelo.ItemDesejo;
import br.com.home.gestor.modelo.StatusItemDesejado;

public interface ItemDesejoRepository extends GenericRepository<ItemDesejo, Long> {

    public List<ItemDesejo> findItensDesejoByStatusIn(Set<StatusItemDesejado> status);
}
