package br.com.home.gestor.repositorio;

import br.com.home.gestor.modelo.TipoPagamento;

public interface TipoPagamentoRepositorio extends GenericRepository<TipoPagamento, Long> {

}
