package br.com.home.gestor.repositorio;

import br.com.home.gestor.modelo.Categoria;

public interface CategoriaRepository extends GenericRepository<Categoria, Long> {

}
