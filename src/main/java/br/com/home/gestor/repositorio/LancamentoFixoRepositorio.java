package br.com.home.gestor.repositorio;

import br.com.home.gestor.modelo.LancamentoFixo;
import br.com.home.gestor.modelo.PeriodicidadeFixa;

import java.util.List;

public interface LancamentoFixoRepositorio extends GenericRepository<LancamentoFixo, Long> {

}
