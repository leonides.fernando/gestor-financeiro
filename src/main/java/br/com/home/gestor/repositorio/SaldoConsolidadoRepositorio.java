package br.com.home.gestor.repositorio;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.home.gestor.modelo.SaldoConsolidado;

public interface SaldoConsolidadoRepositorio extends GenericRepository<SaldoConsolidado, Long> {

	@Query(value="select sc from SaldoConsolidado sc where sc.mes = :mes and sc.ano = :ano")
	public SaldoConsolidado findByMesAno(@Param("mes") int mes, @Param("ano") int ano);
}
