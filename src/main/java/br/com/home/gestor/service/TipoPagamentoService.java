package br.com.home.gestor.service;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import br.com.home.gestor.modelo.TipoPagamento;
import br.com.home.gestor.repositorio.GenericRepository;
import br.com.home.gestor.repositorio.TipoPagamentoRepositorio;

@Named
@Singleton
public class TipoPagamentoService extends GenericService<TipoPagamento, Long> {

	@Inject
	private TipoPagamentoRepositorio tipoPagamentoRepositorio;
	
	@Override
	protected GenericRepository<TipoPagamento, Long> getRepository() {
		return tipoPagamentoRepositorio;
	}

}
