package br.com.home.gestor.service;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.home.gestor.modelo.LancamentoFixo;
import br.com.home.gestor.modelo.PeriodicidadeFixa;
import br.com.home.gestor.repositorio.GenericRepository;
import br.com.home.gestor.repositorio.LancamentoFixoRepositorio;

import java.util.List;

@Named
@Singleton
public class LancamentoFixoService extends GenericService<LancamentoFixo, Long> {

	@PersistenceContext
	private EntityManager entityManager;

	@Inject
	private LancamentoFixoRepositorio lancamentoFixoRepositorio;
	
	@Override
	protected GenericRepository<LancamentoFixo, Long> getRepository() {
		return lancamentoFixoRepositorio;
	}

	public List<LancamentoFixo> findByAtivoTrueAndPeriodicidadeFixa(PeriodicidadeFixa periodicidadeFixa){
		TypedQuery<LancamentoFixo> query =  entityManager.createNamedQuery("lancamentoFixo.findByAtivoAndPeriodicidadeFixa", LancamentoFixo.class)
				.setParameter("periodicidadeFixa", periodicidadeFixa);
		return query.getResultList();
	}
}
