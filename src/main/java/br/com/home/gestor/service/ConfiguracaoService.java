package br.com.home.gestor.service;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.home.gestor.modelo.Configuracao;

@Named
@Singleton
public class ConfiguracaoService {

	@PersistenceContext
	private EntityManager entityManager;
	
	private static final long CONFIGURACAO_ID = 1L;
	
	
	public Configuracao get(){
		return entityManager.getReference(Configuracao.class, CONFIGURACAO_ID);
	}
}
