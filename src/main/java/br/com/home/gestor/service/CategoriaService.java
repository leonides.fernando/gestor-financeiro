package br.com.home.gestor.service;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import br.com.home.gestor.modelo.Categoria;
import br.com.home.gestor.repositorio.CategoriaRepository;
import br.com.home.gestor.repositorio.GenericRepository;

@Named
@Singleton
public class CategoriaService extends GenericService<Categoria, Long> {

	@Inject
	private CategoriaRepository categoriaRepository;
	
	@Override
	protected GenericRepository<Categoria, Long> getRepository() {
		return categoriaRepository;
	}

}
