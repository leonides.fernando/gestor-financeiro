package br.com.home.gestor.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.joda.time.LocalDate;
import org.springframework.transaction.annotation.Transactional;

import br.com.home.gestor.dh.HistoricoGastos;
import br.com.home.gestor.dh.LancamentoCategoria;
import br.com.home.gestor.modelo.Categoria;
import br.com.home.gestor.modelo.Lancamento;
import br.com.home.gestor.modelo.LancamentoFixo;
import br.com.home.gestor.modelo.SaldoConsolidado;
import br.com.home.gestor.modelo.TipoLancamento;
import br.com.home.gestor.repositorio.GenericRepository;
import br.com.home.gestor.repositorio.LancamentoRepositorio;

@Named
@Singleton
public class LancamentoService extends GenericService<Lancamento, Long> {
	
	@Inject
	private LancamentoRepositorio lancamentoRepositorio;
	
	@Inject
	private SaldoConsolidadoService saldoConsolidadoService;
	

	@Override
	protected GenericRepository<Lancamento, Long> getRepository() {
		return lancamentoRepositorio;
	}
	
	public List<Lancamento> findLancamentosNoMes(Date data){
		Date dataInicial = LocalDate.fromDateFields(data).dayOfMonth().withMinimumValue().toDateMidnight().toDate();
		Date dataFinal = LocalDate.fromDateFields(data).dayOfMonth().withMaximumValue().toDateMidnight().toDate();
		return lancamentoRepositorio.findByDataLancamentoBetween(dataInicial, dataFinal);
	}

	@Transactional
	public void registraLancamento(Lancamento lancamento){
		LocalDate dataLancamento = LocalDate.fromDateFields(lancamento.getDataLancamento());
		LocalDate hoje = LocalDate.now();
		if(dataLancamento.isBefore(hoje) || dataLancamento.equals(hoje)){
			lancamento.setPago(true);
		}
		lancamento.setMesAno(dataLancamento.getMonthOfYear(), dataLancamento.getYear());
		lancamentoRepositorio.save(lancamento);
		saldoConsolidadoService.atualizaSaldo(dataLancamento.getMonthOfYear(), dataLancamento.getYear(), lancamento.getValor(), lancamento.getTipoLancamento());
	}

	@Transactional
	public void adicionaLancamentoFixo(Lancamento lancamento){
		registraLancamento(lancamento);
	}

	@Transactional
	public void registraLancamentoParcelado(Lancamento lancamento,
			int parcelaInicial, int numeroParcelas) {

		Date data = lancamento.getDataLancamento();
		LocalDate hoje = LocalDate.now();
		for (int i = parcelaInicial; i <= numeroParcelas; i++) {
			Lancamento novo = Lancamento.novo();
			copiaCategorias(novo, lancamento);
			novo.setPeriodicidadeParcelada(lancamento.getPeriodicidadeParcelada());
			novo.setDataLancamento(data);
			novo.setDescricao(lancamento.getDescricao() + " - " + i + "/"+numeroParcelas);
			novo.setTipoLancamento(lancamento.getTipoLancamento());
			novo.setTipoPagamento(lancamento.getTipoPagamento());
			novo.setValor(lancamento.getValor());
			novo.setPago(hoje.isAfter(LocalDate.fromDateFields(data)));
			novo.setNumeroParcela(i);
			registraLancamento(novo);
			data = novo.getPeriodicidadeParcelada().getProximaDataPorPeriodicidade(data, 1);
		}
	}
	
	protected void copiaCategorias(Lancamento novo, Lancamento lancamento){
		
		for (Categoria categoria : lancamento.getCategorias()) {
			novo.adicionaCategoria(categoria);
		}
	}
	
	public List<Lancamento> findContasAReceber(Date dataInicial, Date dataFinal){
		return lancamentoRepositorio.findContasAReceberBetween(dataInicial, dataFinal);
	}

	public List<Lancamento> findContasAPagar(Date dataInicial, Date dataFinal) {
		return lancamentoRepositorio.findContasAPagar(dataInicial, dataFinal);
	}
	
	public BigDecimal calculaTotalSaidasMes(Date dataInicial, Date dataFinal){
		BigDecimal total = lancamentoRepositorio.calculaTotalPorTipoLancamento(dataInicial, dataFinal, TipoLancamento.SAIDA);
		if(total == null){
			total = BigDecimal.ZERO;
		}
		return total;
	}
	
	public BigDecimal calculaTotalEntradasMes(Date dataInicial, Date dataFinal){
		BigDecimal total = lancamentoRepositorio.calculaTotalPorTipoLancamento(dataInicial, dataFinal, TipoLancamento.ENTRADA);
		if(total == null){
			total = BigDecimal.ZERO;
		}
		return total;
	}
	
	public BigDecimal calculaSaldoMesAnterior(Date dataBase){
		LocalDate data = LocalDate.fromDateFields(dataBase).minusMonths(1);
		int mes = data.getMonthOfYear();
		int ano = data.getYear();

		SaldoConsolidado saldoConsolidado = saldoConsolidadoService.findByMesAno(mes, ano);
		if(saldoConsolidado == null){
			return BigDecimal.ZERO;
		}
		BigDecimal saldo = saldoConsolidado.getValor();
		if(saldo == null){
			return BigDecimal.ZERO;
		}
		return saldo;
	}
	
	public List<LancamentoCategoria> findLancamentosPorPeriodo(Date dataInicial, Date dataFinal){
		return lancamentoRepositorio.findLancamentosPorPeriodo(dataInicial, dataFinal);
	}
	
	public List<HistoricoGastos> findGastosPorIntervaloMeses(int meses){
		LocalDate dataBase = LocalDate.now();
		int mesFinal = dataBase.getMonthOfYear();
		int anoFinal = dataBase.getYear();
		dataBase = dataBase.minusMonths(meses);
		int mesInicial = dataBase.getMonthOfYear();
		int anoInicial = dataBase.getYear();
		
		return lancamentoRepositorio.findGastosPorMesAno(mesInicial, anoInicial, mesFinal, anoFinal);
	}
}
