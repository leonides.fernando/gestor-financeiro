package br.com.home.gestor.service;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import br.com.home.gestor.modelo.ItemDesejo;
import br.com.home.gestor.modelo.StatusItemDesejado;
import br.com.home.gestor.repositorio.GenericRepository;
import br.com.home.gestor.repositorio.ItemDesejoRepository;

import java.util.List;
import java.util.Set;

@Named
@Singleton
public class ItemDesejoService extends GenericService<ItemDesejo, Long> {

	@Inject
	private ItemDesejoRepository repository;
	
	@Override
	protected GenericRepository<ItemDesejo, Long> getRepository() {
		return repository;
	}


    public List<ItemDesejo> findItensDesejoByStatusIn(Set<StatusItemDesejado> status){
        return repository.findItensDesejoByStatusIn(status);
    }

}
