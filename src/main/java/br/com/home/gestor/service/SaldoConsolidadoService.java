package br.com.home.gestor.service;

import java.math.BigDecimal;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.springframework.transaction.annotation.Transactional;

import br.com.home.gestor.modelo.SaldoConsolidado;
import br.com.home.gestor.modelo.TipoLancamento;
import br.com.home.gestor.repositorio.GenericRepository;
import br.com.home.gestor.repositorio.SaldoConsolidadoRepositorio;

@Named
@Singleton
public class SaldoConsolidadoService extends GenericService<SaldoConsolidado, Long> {

	@Inject
	private SaldoConsolidadoRepositorio repositorio;
	
	@Override
	protected GenericRepository<SaldoConsolidado, Long> getRepository() {
		return repositorio;
	}

	@Transactional
	public void atualizaSaldo(int mes, int ano, final BigDecimal valor, final TipoLancamento tipoLancamento){
		SaldoConsolidado saldo = repositorio.findByMesAno(mes, ano);
		if(saldo == null){
			saldo = new SaldoConsolidado();
			saldo.setAno(ano);
			saldo.setMes(mes);
			saldo.setValor(BigDecimal.ZERO);
		}
		
		if(TipoLancamento.ENTRADA.equals(tipoLancamento)){
			saldo.setValor(saldo.getValor().add(valor));			
		}else{
			saldo.setValor(saldo.getValor().subtract(valor));
		}
		repositorio.save(saldo);
	}
	
	public SaldoConsolidado findByMesAno(int mes, int ano){
		return repositorio.findByMesAno(mes, ano);
	}
}
