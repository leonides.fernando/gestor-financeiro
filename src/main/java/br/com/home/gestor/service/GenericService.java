package br.com.home.gestor.service;


import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import br.com.home.gestor.repositorio.GenericRepository;

@Named
public abstract class GenericService<T, PK extends Serializable> {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> findAll() {

		List list = getRepository().findAll();
		if (list == null || list.size() == 0) {
			return list;
		}

		if (list.get(0) instanceof Comparable) {
			Collections.sort(list);
		}

		return list;
	}

	@Transactional
	public T save(T object) {
		return getRepository().save(object);
	}

	public T findById(PK id) {
		return getRepository().findOne(id);
	}

	@Transactional
	public void remove(PK id) {
		getRepository().delete(id);
	}

	@Transactional
	public void remove(Collection<PK> col) {
		for (PK id : col) {
			getRepository().delete(id);
		}
	}
	
	protected abstract GenericRepository<T, PK> getRepository();

}
