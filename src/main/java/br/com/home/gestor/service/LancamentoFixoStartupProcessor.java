package br.com.home.gestor.service;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class LancamentoFixoStartupProcessor implements ApplicationListener<ContextRefreshedEvent> {

    @Inject
    private  LancamentoFixoService lancamentoFixoService;

    @Inject
    private LancamentoService lancamentoService;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

       /* final PeriodicidadeFixa[] periodicidades = PeriodicidadeFixa.values();

        for (PeriodicidadeFixa p : periodicidades){
            final List<LancamentoFixo> lancamentoFixos = lancamentoFixoService.findByAtivoTrueAndPeriodicidadeFixa(p);
            for (LancamentoFixo lancamentoFixo : lancamentoFixos){
                final LocalDate data = LocalDate.fromDateFields(lancamentoFixo.getDataUltimoLancamento());
                if(p.isCriaNovoLancamentoPorData(data)){
                    Lancamento lancamento = Lancamento.fromLancamentoFixo(lancamentoFixo);
                    lancamentoService.adicionaLancamentoFixo(lancamento);

                    lancamentoFixo.ultimoLancamentoHoje();
                    lancamentoFixoService.save(lancamentoFixo);
                }
            }
        }*/
    }
}
