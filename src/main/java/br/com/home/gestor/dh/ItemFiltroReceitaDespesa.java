package br.com.home.gestor.dh;

import br.com.home.gestor.component.MultiChechFilterDescription;

public enum ItemFiltroReceitaDespesa implements MultiChechFilterDescription {

	RECEITAS("Receitas"),
	RECEITAS_PAGAS("Receitas Pagas"),
	RECEITAS_NAO_PAGAS("Receitas não pagas"),
	DESPESAS("Despesas"),
	DESPESAS_PAGAS("Despesas pagas"),
	DESPESAS_NAO_PAGAS("Despesas não pagas");
	
	private String nome;
	
	private boolean selecionado = false;
	
	private ItemFiltroReceitaDespesa(String txt){
		this.nome = txt;
	}
	
	public String getNome() {
		return nome;
	}
	
	public boolean isSelecionado() {
		return selecionado;
	}
	
	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	@Override
	public String getText() {
		return nome;
	}
}
