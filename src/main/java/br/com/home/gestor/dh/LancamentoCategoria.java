package br.com.home.gestor.dh;

import java.math.BigDecimal;

import br.com.home.gestor.modelo.TipoLancamento;

public final class LancamentoCategoria {

	private BigDecimal valor;
	private TipoLancamento tipoLancamento;
	private String categoria;
	
	public LancamentoCategoria(BigDecimal valor, TipoLancamento tipoLancamento,
			String categoria) {
		this.valor = valor;
		this.tipoLancamento = tipoLancamento;
		this.categoria = categoria;
	}

	public BigDecimal getValor() {
		return valor;
	}
	public TipoLancamento getTipoLancamento() {
		return tipoLancamento;
	}
	public String getCategoria() {
		return categoria;
	}
	public boolean isEntrada(){
		return TipoLancamento.ENTRADA.equals(tipoLancamento);
	}
	
}
