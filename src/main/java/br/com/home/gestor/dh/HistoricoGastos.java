package br.com.home.gestor.dh;

import java.math.BigDecimal;

public final class HistoricoGastos {

	private BigDecimal valorMes;
	private String categoria;
	
	public HistoricoGastos(Double valorMes, String categoria) {
		this.valorMes = BigDecimal.valueOf(valorMes);
		this.categoria = categoria;
	}
	
	public BigDecimal getValorMes() {
		return valorMes;
	}
	
	public String getCategoria() {
		return categoria;
	}
}
