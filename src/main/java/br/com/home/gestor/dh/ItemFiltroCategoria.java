package br.com.home.gestor.dh;

import br.com.home.gestor.component.MultiChechFilterDescription;
import br.com.home.gestor.modelo.Categoria;

public final class ItemFiltroCategoria implements MultiChechFilterDescription {

	private Categoria categoria;
	private boolean selecionado;
	
	
	public ItemFiltroCategoria(Categoria categoria){
		this.categoria = categoria;
		this.selecionado = false;
	}
	
	public Categoria getCategoria() {
		return categoria;
	}
	
	public boolean isSelecionado() {
		return selecionado;
	}
	
	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}
	
	@Override
	public String getText() {
		return categoria.getNome();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result + (selecionado ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemFiltroCategoria other = (ItemFiltroCategoria) obj;
		if (categoria == null) {
			if (other.categoria != null)
				return false;
		} else if (!categoria.equals(other.categoria))
			return false;
		if (selecionado != other.selecionado)
			return false;
		return true;
	}
	
}
