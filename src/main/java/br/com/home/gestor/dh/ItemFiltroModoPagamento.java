package br.com.home.gestor.dh;

import br.com.home.gestor.component.MultiChechFilterDescription;
import br.com.home.gestor.modelo.TipoPagamento;
import com.google.common.base.Objects;

public final class ItemFiltroModoPagamento implements MultiChechFilterDescription {

	private TipoPagamento tipoPagamento;
	private boolean selecionado;
	
	
	
	public ItemFiltroModoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
		this.selecionado = false;
	}

	@Override
	public String getText() {
		return tipoPagamento.getNome();
	}

	@Override
	public boolean isSelecionado() {
		return selecionado;
	}
	
	public TipoPagamento getTipoPagamento(){
		return tipoPagamento;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(tipoPagamento, selecionado);
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof ItemFiltroModoPagamento) {
			ItemFiltroModoPagamento that = (ItemFiltroModoPagamento) object;
			return Objects.equal(this.tipoPagamento, that.tipoPagamento)
					&& Objects.equal(this.selecionado, that.selecionado);
		}
		return false;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

}
