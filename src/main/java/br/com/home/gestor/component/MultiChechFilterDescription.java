package br.com.home.gestor.component;

public interface MultiChechFilterDescription {

	String getText();
	
	boolean isSelecionado();
}
