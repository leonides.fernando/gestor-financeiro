package br.com.home.gestor.component;

import javax.faces.component.NamingContainer;
import javax.faces.component.UIInput;

public class MultiCheckFilterList extends UIInput implements NamingContainer {

//	private static final String OPTIMIZED_PACKAGE = "br.com.home.gestor.component.";
	
	protected enum PropertyKeys{
		itemLabel,
		value,
		listener,
		clearSelectedItensListener,
		itemSelect,
		update,
		labelOpen,
		labelClose;
        
        String toString;
        PropertyKeys(String toString) { this.toString = toString; }
        PropertyKeys() { }
        public String toString() {
            return ((toString != null) ? toString : super.toString());
        }
	}
	
    public MultiCheckFilterList() {
        super();
        setRendererType("javax.faces.Group");
    }
    
    @Override
    public String getFamily() {
            return "javax.faces.NamingContainer";
    }

	public String getItemLabel() {
		return (String) getStateHelper().eval(PropertyKeys.itemLabel);
	}

	public void setItemLabel(String itemLabel) {
		getStateHelper().put(PropertyKeys.itemLabel, itemLabel);
	}

	public String getValue() {
		return (String) getStateHelper().eval(PropertyKeys.value);
	}

	public void setValue(String value) {
		getStateHelper().put(PropertyKeys.value, value);
	}

	public String getListener() {
		return (String) getStateHelper().eval(PropertyKeys.listener);
	}

	public void setListener(String listener) {
		getStateHelper().put(PropertyKeys.listener, listener);
	}

	public String getClearSelectedItensListener() {
		return (String) getStateHelper().eval(PropertyKeys.clearSelectedItensListener);
	}

	public void setClearSelectedItensListener(String clearSelectedItensListener) {
		getStateHelper().put(PropertyKeys.clearSelectedItensListener, clearSelectedItensListener );
	}

	public String getItemSelect() {
		return (String) getStateHelper().eval(PropertyKeys.itemSelect);
	}

	public void setItemSelect(String itemSelect) {
		getStateHelper().put(PropertyKeys.itemSelect, itemSelect);
	}

	public String getUpdate() {
		return (String) getStateHelper().eval(PropertyKeys.update);
	}

	public void setUpdate(String update) {
		getStateHelper().put(PropertyKeys.update, update);
	}

	public String getLabelOpen() {
		return (String) getStateHelper().eval(PropertyKeys.labelOpen);
	}

	public void setLabelOpen(String labelOpen) {
		getStateHelper().put(PropertyKeys.labelOpen, labelOpen);
	}

	public String getLabelClose() {
		return (String) getStateHelper().eval(PropertyKeys.labelClose);
	}

	public void setLabelClose(String labelClose) {
		getStateHelper().put(PropertyKeys.labelClose, labelClose);
	}
    

}
